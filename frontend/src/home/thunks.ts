
import {loadMenuSuccess,loadMenuFailed,
        loadPromotionSuccess,loadPromotionFailed,
        IHomeActions,
        loadMenuItemSuccess,
        loadMenuItemFailed,
        loadLocationSuccess,
        loadLocationFailed,
        loadAllLocationSuccess,
        loadAllLocationFailed} from './actions';

import { Dispatch } from 'redux';
import { CallHistoryMethodAction} from 'connected-react-router';
import { ThunkResult } from '../store';

const REACT_APP_API_SERVER = process.env.REACT_APP_API_SERVER;


export function loadMenu():ThunkResult<void>{
    return async (dispatch:Dispatch<IHomeActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/menu`);
        const result = await res.json();
        if (res.status === 200){
            dispatch(loadMenuSuccess(result.menu));
        } else {
            dispatch(loadMenuFailed(result.msg))
        }
    }
}

export function loadPromotion():ThunkResult<void>{
    return async (dispatch:Dispatch<IHomeActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/promotion`);
        const result = await res.json();
        if (res.status === 200){
            dispatch(loadPromotionSuccess(result.promotion));
        } else {
            dispatch(loadPromotionFailed(result.msg));
        }
    }    
}

export function loadMenuItem(id:string):ThunkResult<void>{
    return async (dispatch:Dispatch<IHomeActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/menu/${id}`);
        const result = await res.json();
        const a = console;
        a.log(result);
        if (res.status === 200) {
            dispatch(loadMenuItemSuccess(result));
        } else {
            dispatch(loadMenuItemFailed(result.msg));
        }
    }
}

export function loadLocation(nameOrAddress:string):ThunkResult<void>{
    return async (dispatch:Dispatch<IHomeActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/location`,{
            method:'POST',
            headers:{
                "Content-Type":"application/json"
            },
            body:JSON.stringify({nameOrAddress})
        });
        const result = await res.json();
        // const a = console;
        // a.log(result);
        if (res.status === 200) {
            dispatch(loadLocationSuccess(result.locationOptions));
        } else {
            dispatch(loadLocationFailed(result.msg));
        }
    }
}



export function loadAllLocation():ThunkResult<void>{
    return async (dispatch:Dispatch<IHomeActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/location`);
        const result = await res.json();
        // const a = console;
        // a.log(result);
        if (res.status === 200) {
            dispatch(loadAllLocationSuccess(result.allLocation));
        } else {
            dispatch(loadAllLocationFailed(result.msg));
        }
    }
}