// type loadMenuSuccess = "LOAD_IMG_SUCCESS";
// type loadMenuFailed = "LOAD_IMG_Failed";
// type loadPromotionImgSuccess = "LOAD_PROMOTION_IMG_SUCCESS"
// type loadPromotionImgFailed = "LOAD_PROMOTION_IMG_FAILED"
// type loadPromotionUrlSuccess = "LOAD_PROMOTION_Url_SUCCESS"
// type loadPromotionUrlFailed = "LOAD_PROMOTION_Url_FAILED"

interface IShoppingItemState{
    id:number
    food:string
    quantity:number
    price:number
}

export function selectLocationSuccess(selectedLocation:{}){
    return{
        type: "SELECT_LOCATION_SUCCESS" as "SELECT_LOCATION_SUCCESS" ,
        selectedLocation
    }
}


export function loadMenuSuccess(menu: []) {
    return {
        type: "LOAD_MENU_SUCCESS" as "LOAD_MENU_SUCCESS",
        menu
    }
}

export function loadMenuFailed(menuMsg: string) {
    return {
        type: "LOAD_MENU_FAILED" as "LOAD_MENU_FAILED",
        menuMsg
    }
}


export function loadPromotionSuccess(promotion: any[]) {
    return {
        type: "LOAD_PROMOTION_SUCCESS" as "LOAD_PROMOTION_SUCCESS",
        promotion
    }
}

export function loadPromotionFailed(promotionMsg: string) {
    return {
        type: "LOAD_PROMOTION_FAILED" as "LOAD_PROMOTION_FAILED",
        promotionMsg
    }
}

export function loadMenuItemSuccess(item: {}) {
    return {
        type: "LOAD_ITEM_SUCCESS" as "LOAD_ITEM_SUCCESS",
        item
    }
}


export function loadMenuItemFailed(itemMsg: string) {
    return {
        type: "LOAD_ITEM_FAILED" as "LOAD_ITEM_FAILED",
        itemMsg
    }
}


export function loadAllLocationSuccess(allLocation:string[]){
    return {
        type: "LOAD_ALL_LOCATION_SUCCESS" as "LOAD_ALL_LOCATION_SUCCESS",
        allLocation
    }
}


export function loadAllLocationFailed(locationMsg:string){
    return {
        type: "LOAD_ALL_LOCATION_FAILED" as "LOAD_ALL_LOCATION_FAILED",
        locationMsg
    }
}

export function loadLocationSuccess(searchResult:string[]){
    return {
        type: "LOAD_LOCATION_SUCCESS" as "LOAD_LOCATION_SUCCESS",
        searchResult
    }
}


export function loadLocationFailed(locationSearchMsg:string){
    return {
        type: "LOAD_LOCATION_FAILED" as "LOAD_LOCATION_FAILED",
        locationSearchMsg
    }
}

export function addPurchaseItemSuccess(purchaseItem:IShoppingItemState){
    return{
        type: "ADD_PURCHASE_ITEM_SUCCESS" as "ADD_PURCHASE_ITEM_SUCCESS",
        purchaseItem
    }
}

export function addLocationSuccess (locationId:number,selectedLocation:{}){
    return {
        type: "ADD_LOCATION_SUCCESS" as "ADD_LOCATION_SUCCESS",
        locationId,
        selectedLocation
    }
}

export function removePurchaseItemSuccess(purchaseItems:any[]){
    return{
        type: "REMOVE_PURCHASE_ITEM_SUCCESS" as "REMOVE_PURCHASE_ITEM_SUCCESS",
        purchaseItems
    }
}


export function clearSearchResult(){
    return {
        type: "CLEAR_SEARCH_RESULT" as "CLEAR_SEARCH_RESULT"
    }
}

export function shoppingListReminder(shoppingListMsg:string){
    return{
        type: "SHOPPING_LIST_MSG" as "SHOPPING_LIST_MSG",
        shoppingListMsg
    }
}


export function clearShoppingListReminder(){
    return {
        type: "CLEAR_SHOPPING_LIST_MSG" as "CLEAR_SHOPPING_LIST_MSG"
    }
}

// export function loadPromotionUrlSuccess(promotionUrl:string){
//     return {
//         type: "LOAD_PROMOTION_URL_SUCCESS" as "LOAD_PROMOTION_URL_SUCCESS",
//         promotionUrl
//     }
// }

// export function loadPromotionUrlFailed(promotionMsg:string){
//     return {
//         type: "LOAD_PROMOTION_Url_FAILED" as "LOAD_PROMOTION_Url_FAILED" ,
//         promotionMsg
//     }
// }

// export function loadPromotionImgSuccess(promotionImg:string){
//     return {
//         type: "LOAD_PROMOTION_IMG_SUCCESS" as "LOAD_PROMOTION_IMG_SUCCESS" ,
//         promotionImg
//     }
// }

// export function loadPromotionImgFailed(promotionMsg:string){
//     return {
//         type: "LOAD_PROMOTION_IMG_FAILED" as "LOAD_PROMOTION_IMG_FAILED" ,
//         promotionMsg
//     }
// }



export type IHomeActions = ReturnType<typeof loadMenuSuccess |
                                        typeof loadMenuFailed |
                                        typeof loadPromotionSuccess|
                                        typeof loadPromotionFailed |
                                        typeof loadMenuItemSuccess |
                                        typeof loadMenuItemFailed |
                                        typeof loadLocationSuccess |
                                        typeof loadLocationFailed |
                                        typeof clearSearchResult |
                                        typeof loadAllLocationSuccess |
                                        typeof loadAllLocationFailed |
                                        typeof addPurchaseItemSuccess| 
                                        typeof removePurchaseItemSuccess |
                                        typeof addLocationSuccess |
                                        typeof shoppingListReminder |
                                        typeof clearShoppingListReminder
                                       >
