import {IHomeActions} from './actions';


let initialPurchaseItems;
const temp = sessionStorage.getItem('ShoppingList')
if (temp === null || temp === undefined){
    initialPurchaseItems = [];
    
} else{
    initialPurchaseItems = JSON.parse(temp);
} // sessionStorage.getItem('ShoppingList')?JSON.parse(sessionStorage.getItem('ShoppingList')):[]

const initialState = {
    menu:[] as any,
    promotion:[] as any,
    item: {},
    purchaseItems:initialPurchaseItems,
    searchResult:[],
    allLocation:[],
    locationId:-1,
    selectedLocation:{},
    menuMsg:'',
    promotionMsg:'',
    itemMsg:'',
    locationMsg:'',
    locationSearchMsg:'',
    shoppingListMsg:''
}

export function homeReducers (state = initialState,action:IHomeActions){
    switch (action.type){
        case 'LOAD_MENU_SUCCESS':
            return {...state,menu:action.menu};
        case 'LOAD_MENU_FAILED':
            return {...state,menumMsg:action.menuMsg};
        case 'LOAD_PROMOTION_SUCCESS':
            return {...state, promotion:action.promotion};
        case 'LOAD_PROMOTION_FAILED':
            return {...state,msg:action.promotionMsg};
        case 'LOAD_ITEM_SUCCESS':
            return {...state,item:action.item};
        case 'LOAD_ITEM_FAILED':
            return {...state,itemMsg:action.itemMsg};
        case 'LOAD_LOCATION_SUCCESS':
            return {...state,searchResult:action.searchResult}
        case 'LOAD_LOCATION_FAILED':
            return {...state,locationSearchMsg:action.locationSearchMsg}
        case 'LOAD_ALL_LOCATION_SUCCESS':
            return {...state,allLocation:action.allLocation}
        case 'LOAD_ALL_LOCATION_FAILED':
            return {...state,locationMsg:action.locationMsg}
        case 'CLEAR_SEARCH_RESULT':
            return {...state,searchResult:[]}
        case 'ADD_PURCHASE_ITEM_SUCCESS':
            const purchaseItems = (state.purchaseItems).concat();
            const newPurchaseItem = {...action.purchaseItem}
            for (const item of purchaseItems) {
                if (item.id === newPurchaseItem.id){
                    item.quantity += newPurchaseItem.quantity;
                    item.price += newPurchaseItem.price;
                    const a = console;
                    a.log(item);
                    sessionStorage.setItem('ShoppingList',JSON.stringify(purchaseItems))
                    return {...state,purchaseItems};
                } else{
                    continue;
                }
            };
            sessionStorage.setItem('ShoppingList',JSON.stringify(purchaseItems.concat(action.purchaseItem)));
            return {...state,
                 purchaseItems: purchaseItems.concat(action.purchaseItem)
            }
        case 'REMOVE_PURCHASE_ITEM_SUCCESS':
            if (action.purchaseItems === []){
                sessionStorage.setItem('ShoppingList',[] as any);
            } else {
                sessionStorage.setItem('ShoppingList',JSON.stringify(action.purchaseItems))    
            }
            
            return {...state,purchaseItems};
        case 'ADD_LOCATION_SUCCESS':
            sessionStorage.setItem('selectedLocation', JSON.stringify(action.selectedLocation));
            return {...state,locationId:action.locationId,selectedLocation:action.selectedLocation};
        case 'SHOPPING_LIST_MSG':
            return {...state,shoppingListMsg:action.shoppingListMsg}
        case 'CLEAR_SHOPPING_LIST_MSG':
            return {...state,shoppingListMsg:''}
        default:
            return state;
    }
}