interface IHomeState{
    menu:any[]
    promotion:any[],
    item: {
        id?: number
        food?: string
        img_url?: string
        order_deadline?: string
        kol_intro_video?:string
        kol_fb_link?: string
        kol_ig_link?: string
        price?: number
    },
    locationId:number,
    selectedLocation:{},
    purchaseItems:any[],
    allLocation:string[],
    searchResult:string[],
    locationMsg:string,
    locationSearchMsg:string,
    menuMsg:string,
    promotionMsg:string
    itemMsg:string,
    shoppingListMsg:string
}