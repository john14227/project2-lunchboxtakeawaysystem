import * as React from 'react';
import { match } from 'react-router-dom';
// import { MenuItem } from './MenuItem';
import { Table } from 'reactstrap';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { loadMenu } from './home/thunks';
// import { menuItemDescription } from './MenuItemDescription';
import './Menu.css'

interface IMenuProps {
    match: match<{ id: string }>
    history: {
        push: (url: string) => void
    }
    menu: any[]
    menuMsg: string
    loadMenu: () => void
}

class Menu extends React.Component<IMenuProps>{

    constructor(props: IMenuProps) {
        super(props)
    }

    private goToMenuItem = (id: number) => {
        this.props.history.push(`/menu/${id}`);
    }

    // private goToMoreOptions = () => {
    //     this.props.history.push('/menu');
    // }

    public async componentDidMount() {
        await this.props.loadMenu();
    }


    public render() {
        return (
            <div>
                <h1>菜單</h1>
                <Table>

                    <tbody>
                        {this.props.menu.map((item, i: number) => (
                            <tr key={i}>
                                <th><div className='item-name' onClick={this.goToMenuItem.bind(this, item.id)}>
                                    {item.food}
                                    <br />
                                    ${item.price}
                                    </div>
                                </th>
                                <th><img className="item-img" src={item.img_url} /></th>
                            </tr>
                        ))}
                    </tbody>
                </Table>
                {/* <Route path={`/menu/:id`} exact={true} component={menuItemDescription} /> */}

            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    const { menu, menuMsg } = state.home
    return {
        menu,
        menuMsg,
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadMenu: () => dispatch(loadMenu())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Menu)