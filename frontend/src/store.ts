import {createStore,applyMiddleware,
    compose,combineReducers} from 'redux';
import {createLogger} from 'redux-logger';
import {IAuthState} from './auth/state'
import {authReducer} from './auth/reducers'
import { IAuthActions } from './auth/actions';
import thunk,{ThunkAction,ThunkDispatch} from 'redux-thunk';
import { createBrowserHistory } from 'history';
import {routerMiddleware, connectRouter, RouterState} from 'connected-react-router';
import { homeReducers } from './home/reducers';
import {userReducers} from './user/reducers';
import { IHomeActions } from './home/actions';
import { IUserState } from './user/state';



declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}


export interface IRootState{
    auth: IAuthState
    home: IHomeState
    user:IUserState
    router: RouterState
}

type IRootActions = IHomeActions|IAuthActions

export const history = createBrowserHistory();


const rootReducer = combineReducers<IRootState>({
    auth: authReducer,
    home: homeReducers,
    user: userReducers,
    router: connectRouter(history)
})

export type ThunkResult<R> = ThunkAction<R, IRootState, null, IRootActions>

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const logger = createLogger({
    predicate:(getState, action) => action.type 
})

export default createStore<IRootState,IRootActions,{},{}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    ));


