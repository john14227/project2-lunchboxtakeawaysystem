import * as React from 'react';
import Select from 'react-select';
import { ThunkDispatch, IRootState } from './store';
import { loadAllLocation } from './home/thunks';
import { connect } from 'react-redux';
import { addLocationSuccess } from './home/actions';

// const options = [
//   { value: 'chocolate', label: 'Chocolate' },
//   { value: 'strawberry', label: 'Strawberry' },
//   { value: 'vanilla', label: 'Vanilla' },
//   { value: 'cameral', label: 'cameral' },
//   { value: 'cream', label: 'cream' }
// ];


interface ISearchProps {
    loadAllLocation: () => void
    addLocationSuccess:(locationId:number,selectedLocation:{}) => void
    allLocation: string[]
}

interface ISearchState {
    selectedOption: any
    Options: any[]
    locationId:number
}

class Search extends React.Component<ISearchProps, ISearchState>{
    constructor(props: any) {
        super(props)
        this.state = {
            selectedOption: null,
            Options: [],
            locationId:0
        }
    }

    public async componentWillMount() {
        await this.props.loadAllLocation();
        const a = console;
        a.log(this.props.allLocation);
        const allLocation: any = this.props.allLocation.slice();
        allLocation.forEach((option: any) => {
            option.label = (`${option.name}  (${option.address})`)
        });
        this.setState({
            Options: allLocation.slice()
        });
    }

    private handleChange = (selectedOption: any) => {
        this.setState({ selectedOption });
        // console.log(`Option selected:`, selectedOption);
        const a = console;
        a.log(`Option selected:`, selectedOption);
        sessionStorage.setItem('selectedLocation',selectedOption);
        let locationId = 0;
        if (selectedOption === null || undefined){
            locationId = 0
        } else {
            locationId = selectedOption.id
        }
        this.setState({
            locationId
        },()=>{

            this.props.addLocationSuccess(this.state.locationId,selectedOption)
        })
        
    }

    private getOptionValue = (option: any) => (
        (`${option.name} (地址: ) ${option.address}`)
    )

    private loadingMessage = () => {
        return 'loading'
    }

    public render() {
        const { selectedOption } = this.state;

        return (
            <Select
                value={selectedOption}
                onChange={this.handleChange}
                options={this.state.Options}
                placeholder='在此搜尋及選擇自取點'
                isClearable={true}
                getOptionValue={this.getOptionValue}
                loadingMessage={this.loadingMessage}
            />
        );
    }
}


const mapStateToProps = (state: IRootState) => {
    const allLocation = state.home.allLocation
    return {
        allLocation
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadAllLocation: () => dispatch(loadAllLocation()),
        addLocationSuccess: (locationId:number,selectedLocation:{})=>dispatch(addLocationSuccess(locationId,selectedLocation))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);