import * as React from 'react';
import GoogleMapReact from 'google-map-react';
import { IRootState, ThunkDispatch } from './store';
import { loadAllLocation } from './home/thunks';
import { connect } from 'react-redux';
// import Marker from './Marker'
// import { ThunkDispatch, IRootState } from './store';
// import { loadAllLocation } from './home/thunks';
// import { connect } from 'react-redux';

// const InfoBox = () => 
//     <div style={{
//         color: 'white',
//         background: 'red',
//         padding: '15px 10px',
//         display: 'inline-flex',
//         textAlign: 'center',
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderRadius: '100%',
//         transform: 'translate(-50%, -50%)'}}>
//         Hello world
//     </div>

// const AnyReactComponent = ({ text }: any) =>
//     <div style={{
//         color: 'white',
//         background: 'grey',
//         padding: '15px 10px',
//         display: 'inline-flex',
//         textAlign: 'center',
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderRadius: '100%',
//         transform: 'translate(-50%, -50%)'
//     }} onClick={}>
//         {text}
//     </div>





interface IMapProps {
    center?: ICoords;
    zoom?: number;
    allLocation: string[]
    loadAllLocation: () => void
}

interface ICoords {
    lat: number;
    lng: number;
}

interface IMapState {
    closestPoint: ICoords
    currentPosition: ICoords
    isShown: boolean
    allLocation: string[]
}

class Map extends React.Component<IMapProps, IMapState>{

    constructor(props: any) {
        super(props)
        this.state = {
            isShown: false,
            currentPosition: {
                lat: 0,
                lng: 0
            },
            closestPoint: {
                lat: 0,
                lng: 0
            },
            allLocation: []
        }
    }

    private rad = (x: number): number => {
        return x * Math.PI / 180;
    };

    private getDistance = (p1: any, p2: any): number => {
        const R = 6378137; // Earth’s mean radius in meter
        const dLat = this.rad(p2.lat - p1.lat);
        const dLong = this.rad(p2.lng - p1.lng);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) * Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c;
        return d; // returns the distance in meter
    };


    private success = (position: any) => {
        const currentLat = position.coords.latitude;
        const currentLng = position.coords.longitude;
        const currentPosition = {
            lat: currentLat,
            lng: currentLng
        }

        const allLocation: any[] = this.state.allLocation.slice();
        if (allLocation.length > 0) {
            const temp = {
                lat: Number(allLocation[0].x_latlng),
                lng: Number(allLocation[0].y_latlng)
            }
            const closestPickUpPoint = {
                distance: this.getDistance(currentPosition, temp),
                pickUpPointId: allLocation[0].id,
                lat: allLocation[0].x_latlng,
                lng: allLocation[0].y_latlng
            }
            this.props.allLocation.forEach((location: any) => {
                const a = console;
                a.log(location.id);

                const selectedPoint = {
                    lat: Number(location.x_latlng),
                    lng: Number(location.y_latlng)
                }
                console.log('currentPosition', currentPosition)
                console.log('selectedPoint', selectedPoint);
                const distance = this.getDistance(currentPosition, selectedPoint);
                if (distance < closestPickUpPoint.distance) {
                    closestPickUpPoint.distance = distance
                    closestPickUpPoint.pickUpPointId = location.id
                    closestPickUpPoint.lat = location.x_latlng
                    closestPickUpPoint.lng = location.y_latlng
                }
                a.log(closestPickUpPoint);
            });
            this.setState({
                currentPosition: {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                },
                closestPoint: {
                    lat: closestPickUpPoint.lat,
                    lng: closestPickUpPoint.lng
                }
            });
        }

        const b = console;
        b.log(currentPosition);

        //    <AnyReactComponent 
        //     lat={position.coords.latitude}
        //     lng={position.coords.longtitude}
        //     text={'current position'}
        // />

    }

    // private onClick = () =>{
    //     return (
    //         <div>
    //             a
    //             {/* {address} */}
    //         </div>
    //     )
    // }

    // private renderMarkers(map:any,maps:any){
    //     let marker = new maps.Marker({
    //         position:myLatLng,
    //         map,
    //         title
    //     })
    // }

    // private showBox = () => {
    //     this.setState({
    //         isShown:!this.state.isShown
    //     })
    // }

    private options: {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    public async componentWillMount() {
        await this.props.loadAllLocation();
        this.setState({
            allLocation: this.props.allLocation
        }, () => {
            navigator.geolocation.getCurrentPosition(this.success, undefined, this.options)
        })
    }

    // public async componentDidMount(){
    //     navigator.geolocation.getCurrentPosition(this.success, undefined, this.options)
    // }


    private renderMarkers = (map: any, maps: any) => {
        // const infowindow = new maps.InfoWindow({
        //     content: 'Hello'
        // });

        // const marker = new maps.Marker({
        //     position: {lat:22.367381,lng:113.996105},
        //     map,
        //     title: 'Hello World'
        //   });    
        //   marker.addListener('click',()=>{
        //     infowindow.open(map, marker);
        // })  
        // return marker;


        const allLocation = this.props.allLocation;
        const coords: any[] = [];

        allLocation.forEach((location: any) => {
            coords.push({
                lat: location.x_latlng,
                lng: location.y_latlng,
                name: location.name,
                address: location.address
            })
        })
        const markers: any[] = []
        for (const coord of coords) {

            const name = coord.name
            const address = coord.address
            const infowindow = new maps.InfoWindow({
                content: `${name} (地址: ${address})`
            });

            const marker = new maps.Marker({
                position: { lat: coord.lat, lng: coord.lng },
                map,
                title: name
            });
            marker.addListener('click', () => {
                infowindow.open(map, marker);
            });
            markers.push(marker);
        };

        // if (this.state.currentPosition.lat !== 0 && this.state.currentPosition.lng !== 0){
        //     const infowindow = new maps.InfoWindow({
        //         content: 'current'
        //     });
        //     const marker = new maps.Marker({
        //         position: { lat: this.state.currentPosition.lat, lng: this.state.currentPosition },
        //         map,
        //         title: 'current'
        //     });
        //     marker.addListener('click', () => {
        //         infowindow.open(map, marker);
        //     })
        //     markers.push(marker);
        // }

        return markers;

        // const marker = new maps.Marker({
        //   position: {lat:22.367381,lng:113.996105},
        //   map,
        //   title: 'Hello World!'
        // });
    }


    public render() {
        // let initialClosestPoint;
        // const temp = sessionStorage.getItem('selectedLocation');
        // if (temp === null || temp === undefined){
        //     initialClosestPoint = [];

        // } else{
        //     initialPurchaseItems = JSON.parse(temp);
        // } 
        // const closestPickUpPoint = this.state.closestPoint
        const currentPosition = this.state.currentPosition
        const temp = ({ map, maps }: any) => this.renderMarkers(map, maps)
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '70vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyCNg9dEyRFangtG0YG_XMzodHWIdBYVYl4' }}
                    defaultCenter={
                        {
                            lat: 22.28552,
                            lng: 114.15769
                        }
                    }
                    defaultZoom={20}
                    zoom={11}
                    center={currentPosition}
                    // {...temp.map(location)=>()}
                    onGoogleApiLoaded={temp}
                // onGoogleApiLoaded={({map,maps})=>this.renderMarkers(map,maps)}
                >


                    {/* <Marker 
                        lat={this.state.currentPosition.lat}
                        lng={this.state.currentPosition.lng}
                        name='你目前的所在地'
                        address='你目前的所在地'
                        /> */}

                    {/* <Marker
                        showBox={this.showBox}
                        lat={22.374}
                        lng={113.993}        
                    />

                    
                    {this.state.isShown? <InfoBox /> :""} */}
                    {/* {this.props.allLocation.map((location:any) => 
                        <Marker 
                        lat={location.x_latlng}
                        lng={location.y_latlng}
                        name={location.name}
                        address={location.address}
                        key={location.id}
                        />
                    )} */}
                </GoogleMapReact>
            </div>
        );
    }
}

// interface IMarkerProps{
//     showBox:()=>void
// }

// const Marker = (props:IMarkerProps)=>(
//         <div onClick={props.showBox} style={{
//             color: 'white',
//             background: 'grey',
//             padding: '15px 10px',
//             display: 'inline-flex',
//             textAlign: 'center',
//             alignItems: 'center',
//             justifyContent: 'center',
//             borderRadius: '100%',
//             transform: 'translate(-50%, -50%)'
//         }} 
//             >
//             marker  
//         </div>
// )

const mapStateToProps = (state: IRootState) => {
    const allLocation = state.home.allLocation
    return {
        allLocation
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return{
        loadAllLocation: () => dispatch(loadAllLocation())
    }
}


export default connect(mapStateToProps,mapDispatchToProps) (Map);

// export default Map;
