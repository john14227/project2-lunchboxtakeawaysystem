import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';


interface IItemProps {
    item: {
        id: number
        food: string
        img_url: string
        order_deadline: string
        kol_intro_video: string
        kol_fb_link: string
        kol_ig_link: string
        price: number
    }
}

class Item extends React.Component<IItemProps & RouteComponentProps<{ id: string }>> {
    public render() {
        return (
            <div>
                <div id="menu-item">
                    <div >
                        <div><img src={this.props.item.img_url} alt={this.props.item.food} className="menu-item-img" id={`${this.props.item.id}`} /></div>

                        <div>{this.props.item.food}</div>
                        <div>${this.props.item.price}</div>
                        <div><Link to="`/menu/${this.props.item.id}`}">查看</Link></div>
                        <div><Link to="/">訂購</Link></div>
                    </div>

                </div>
            </div>
        )

    }
}


export const MenuItemPreview = withRouter(Item);
