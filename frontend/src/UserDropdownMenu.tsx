import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './UserDropdownMenu.css';
// import { Provider } from 'react-redux';
// import store from '../store'
// import { connect } from 'react-redux';
import { BrowserRouter, Link } from 'react-router-dom';
import { ThunkDispatch, IRootState } from './store';
import { loadUser } from './user/thunks';
import { connect } from 'react-redux';
// import { PrivateRoute } from './PrivateRoute';
// import UserProfile from './UserProfile';
// import OrderHistory from './userpage/OrderHistory';


// import { NoMatch } from './NoMatch';
// import Home from './Home';
// import { Myorders } from './userpage/Myorders';
// import { Help } from './userpage/Help';
// import { Logout } from './Logout';

interface IUserDropdownMenuProps {
    logout: () => void
    history: {
        push: (url: string) => void
    }
    user: { username: string }
    loadUser: () => void
}

interface IUserDropdownMenuState {
    displayMenu: boolean;
    username: string
}

class UserDropdownMenu extends React.Component<IUserDropdownMenuProps, IUserDropdownMenuState> {
    constructor(props: any) {
        super(props);

        this.state = {
            displayMenu: false,
            username: ''
        };

        this.showDropdownMenu = this.showDropdownMenu.bind(this);
        this.hideDropdownMenu = this.hideDropdownMenu.bind(this);

    };

    private goToUserProfile = () => {
        this.props.history.push('/profile');
    }

    private goToOrderHistory = () => {
        this.props.history.push('/orderhistory');
    }

    public showDropdownMenu = (event: any) => {
        event.preventDefault();
        this.setState({ displayMenu: true }, () => {
            document.addEventListener('click', this.hideDropdownMenu);
        });
    }

    public hideDropdownMenu = () => {
        this.setState({ displayMenu: false }, () => {
            document.removeEventListener('click', this.hideDropdownMenu);
        });

    }

    public async componentWillMount() {
        await this.props.loadUser();
        this.setState({
            username: this.props.user.username
        })
    }

    public render() {
        return (
            <div className="dropdown" style={{ width: "auto", height: "auto" }} >
                <div id ='user-bar'>
                    <h4 style={{color:'blue'}}>你好, {this.state.username}</h4>
                    <div className="button" onClick={this.showDropdownMenu}> 選單 </div>
                </div>

                {this.state.displayMenu ? (
                    <div>
                        <BrowserRouter>
                            <nav>
                                <Link to="/" className="link">Home</Link><br />
                                <Link to="/orderhistory" className="link" onClick={this.goToOrderHistory}>我的訂單</Link><br />
                                <Link to="/profile" className="link" onClick={this.goToUserProfile}>個人檔案</Link><br />
                                {/* <Link to="/help" className="link">尋求幫助</Link><br /> */}
                                <Link to="/logout" className="link" onClick={this.props.logout}>登出</Link>
                            </nav>
                            {/* <PrivateRoute path='/orderhistory' exact={true} component={OrderHistory} />
                            <PrivateRoute path='/profile' exact={true} component={UserProfile} /> */}
                            {/* <div className="menu">
                                <div className="user-menu">
                                    <Switch>
                                        <Route path="/" exact={true} component={Home} />
                                        <Route path="/myorders" component={Myorders} />
                                        <Route path="/help" component={Help} />
                                        {/* <Route path="/logout" component={Logout} /> */}
                            {/* <Route component={NoMatch} /> */}
                            {/* </Switch>
                    </div>
                            </div> */}
                        </BrowserRouter>
                    </div >
                ) :
                    (
                        null
                    )
                }

            </div>

        );
    }
}


const mapStateToProps = (state: IRootState) => {
    const user = state.user.user
    return {
        user
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadUser: () => dispatch(loadUser())

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDropdownMenu)
// export default UserDropdownMenu;