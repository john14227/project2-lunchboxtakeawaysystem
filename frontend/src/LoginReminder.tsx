import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
// import { Link } from 'react-router-dom';


interface ILoginFailModalProps {
    // open: boolean;
    close: () => void
    history: {
        push: (url: string) => void
    }
}


export class LoginReminder extends React.Component< ILoginFailModalProps> {

    private goToLoginPage = () =>{
        this.props.history.push('/login')
    }

    public render (){
        return (
            <Modal isOpen={true}>
    
                <ModalHeader> 提示 </ModalHeader>
                <ModalBody>
                    請先<span onClick={this.goToLoginPage}>登入</span>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.props.close}>Close</Button>
                    {/* </Link> */}
    
                </ModalFooter>
    
            </Modal>
        )
    }
}

