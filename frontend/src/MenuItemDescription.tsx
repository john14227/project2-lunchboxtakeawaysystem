import * as React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './MenuItemDescription.css';
import { IRootState, ThunkDispatch } from './store'
import { connect } from 'react-redux';
import { match } from 'react-router-dom';
import { loadMenuItem } from './home/thunks';
import { Container, Row, Col, Button } from 'reactstrap';
import ItemPurchaseModal from './ItemPurchaseModal';
import { LoginReminder } from './LoginReminder';


interface IItemDescriptionProps {
    match: match<{ id: string }>
    history: {
        push: (url: string) => void
    }
    item: {
        id: number
        food: string
        img_url: string
        order_deadline: string
        kol_intro_video: string
        kol_fb_link: string
        kol_ig_link: string
        price: number
    }
    itemMsg: string,
    loadMenuItem: (id: string) => void
}


interface IItemDescriptionState {
    isPurchaseBoxShown: boolean
    isLoginFirstReminderShown: boolean
}

class ItemDescription extends React.Component<IItemDescriptionProps, IItemDescriptionState>{
    constructor(props: IItemDescriptionProps) {
        super(props);
        this.state = {
            isPurchaseBoxShown: false,
            isLoginFirstReminderShown: false
        }
    }

    private returnToMenu = () => {
        this.props.history.push('/menu');
    }


    private openItemtoPurchaseBox = () => {
        this.setState({
            isPurchaseBoxShown: true
        });
    }


    private openLoginReminder = () => {
        this.setState({
            isLoginFirstReminderShown: true
        })
    }

    private closePurchaseBox = () => {
        this.setState({
            isPurchaseBoxShown: false
        });
    }

    private closeLoginReminder = () => {
        this.setState({
            isLoginFirstReminderShown: false
        })
    }

    private goToPurchasePage = () => {
        if (localStorage.getItem('token') === null) {
            this.openLoginReminder();
        }
        else {
            this.props.history.push('/purchase')
        }
    }


    public async componentDidMount() {
        const id = this.props.match.params.id;
        await this.props.loadMenuItem(id);
    }

    public render() {

        return (
            <div>
                <Container>
                    <Row>
                        <Col sm='12' className='item-border'>
                            <div className="item-img">
                                <img src={this.props.item.img_url} />
                            </div>
                            <div>
                                <span>{this.props.item.food}</span>
                                <span>{this.props.item.price}</span>
                            </div>

                            <Button onClick={this.openItemtoPurchaseBox}>加入購物車</Button>
                            <Button onClick={this.goToPurchasePage}>查看購物車</Button>
                            <Button onClick={this.returnToMenu}>返回</Button>
                            {this.state.isPurchaseBoxShown && <ItemPurchaseModal
                                item={this.props.item}
                                close={this.closePurchaseBox}
                            />}

                            {this.state.isLoginFirstReminderShown && <LoginReminder
                                close={this.closeLoginReminder}
                                history={this.props.history}
                            />}
                        </Col>
                    </Row>

                </Container>

            </div>
        )
    }
}



const mapStateToProps = (state: IRootState) => {
    const { item, itemMsg } = state.home
    return {
        item,
        itemMsg
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadMenuItem: (id: string) => dispatch(loadMenuItem(id))
    }
}

export const menuItemDescription = connect(mapStateToProps, mapDispatchToProps)(ItemDescription);
