export function loadUserSuccess(user:{}) {
    return {
        type: 'LOAD_USER_SUCCESS' as 'LOAD_USER_SUCCESS',
        user
    }
}

export function loadUserFailed(msg: string) {
    return {
        type: 'LOAD_USER_FAILED' as 'LOAD_USER_FAILED',
        msg
    }
}

export function updateSuccess(msg: string) {
    return {
        type: 'UPDATE_SUCCESS' as 'UPDATE_SUCCESS',
        msg
    }
}

export function updateFailed(msg: string) {
    return {
        type: 'UPDATE_FAILED' as 'UPDATE_FAILED',
        msg
    }
}

export function loadUserOrderHistorySuccess(userOrderHistory:any[]) {
    return {
        type: 'LOAD_USER_ORDER_HISTORY_SUCCESS' as 'LOAD_USER_ORDER_HISTORY_SUCCESS',
        userOrderHistory
    }
}


export function loadUserOrderHistoryFailed(msg: string) {
    return {
        type: 'LOAD_USER_ORDER_HISTORY_FAILED' as 'LOAD_USER_ORDER_HISTORY_FAILED',
        msg
    }
}


export function submitOrderSuccess(msg: string) {
    return {
        type: 'SUBMIT_ORDER_SUCCESS' as 'SUBMIT_ORDER_SUCCESS',
        msg
    }
}


export function submitOrderFailed(msg: string) {
    return {
        type: 'SUBMIT_ORDER_FAILED' as 'SUBMIT_ORDER_FAILED',
        msg
    }
}


export type IUserActions = ReturnType<
    typeof loadUserSuccess|
    typeof loadUserFailed |
    typeof updateSuccess |
    typeof updateFailed |
    typeof loadUserOrderHistorySuccess |
    typeof loadUserOrderHistoryFailed |
    typeof submitOrderSuccess |
    typeof submitOrderFailed>