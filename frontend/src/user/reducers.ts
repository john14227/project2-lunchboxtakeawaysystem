import {IUserActions} from './actions';

const initialState = {
    user:{},
    userOrderHistory:[] as any,
    msg:''
}

export function userReducers (state = initialState,action:IUserActions){
    switch(action.type){
        case 'LOAD_USER_SUCCESS':
            return {...state,user:action.user}
        case 'LOAD_USER_FAILED':
            return {...state,msg:action.msg}
        case 'UPDATE_SUCCESS':
            return {...state,msg:action.msg}
        case 'UPDATE_FAILED':
            return {...state,msg:action.msg}
        case 'SUBMIT_ORDER_SUCCESS':
            sessionStorage.clear();
            return {...state,msg:action.msg}
        case 'SUBMIT_ORDER_FAILED':
            return {...state,msg:action.msg}
        case 'LOAD_USER_ORDER_HISTORY_SUCCESS':
            return {...state,userOrderHistory:action.userOrderHistory}
        case 'LOAD_USER_ORDER_HISTORY_FAILED':
            return {...state,msg:action.msg}
        default:
            return state
    }
}

