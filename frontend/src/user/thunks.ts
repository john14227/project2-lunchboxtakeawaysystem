import { Dispatch } from 'redux';
import { IUserActions, loadUserOrderHistorySuccess, loadUserOrderHistoryFailed, submitOrderSuccess, submitOrderFailed } from 'src/user/actions';
import { CallHistoryMethodAction } from 'connected-react-router';
import { updateSuccess, updateFailed, loadUserSuccess, loadUserFailed } from './actions';
import {authorizedFetch} from '../common/fetch';
import { shoppingListReminder, IHomeActions } from 'src/home/actions';

const REACT_APP_API_SERVER = process.env.REACT_APP_API_SERVER;


export function update(user: any) {
    return async (dispatch: Dispatch<IUserActions | CallHistoryMethodAction>) => {
        const res = await authorizedFetch(`${REACT_APP_API_SERVER}/user/currentUser`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({user})
        });
        const result = await res.json();
        if (res.status === 200) {
            dispatch(updateSuccess(result.msg));
        } else {
            dispatch(updateFailed(result.msg));
        }
    }
}

export function loadUser(){
    return async (dispatch: Dispatch<IUserActions | CallHistoryMethodAction>) => {
        const res = await authorizedFetch(`${REACT_APP_API_SERVER}/user/currentUser`);
        const result = await res.json();

        if (res.status === 200) {
            dispatch(loadUserSuccess(result.user));
        } else {
            dispatch(loadUserFailed(result.msg));
        }
    }
}

export function loadUserOrderHistory(){
    return async (dispatch: Dispatch<IUserActions | CallHistoryMethodAction>) => {
        const res = await authorizedFetch (`${REACT_APP_API_SERVER}/orders/currentUser`);
        const result = await res.json();
        if (res.status === 200) {
            dispatch(loadUserOrderHistorySuccess(result.orders));
        } else {
            dispatch(loadUserOrderHistoryFailed(result.msg));
        }
    }
}

export function submitOrder(order:any){
    return async (dispatch: Dispatch<IUserActions | IHomeActions | CallHistoryMethodAction>) => {
        const res = await authorizedFetch(`${REACT_APP_API_SERVER}/orders/submit`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({order})
        });
        const result = await res.json();
        if (res.status === 200) {
            dispatch(submitOrderSuccess(result.msg));
            dispatch(shoppingListReminder(result.msg));
        } else {
            dispatch(submitOrderFailed(result.msg));
        }
    }
}
