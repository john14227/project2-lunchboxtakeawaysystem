import * as React from 'react';
import { login } from './auth/thunks';
import { ThunkDispatch, IRootState } from './store';
import { connect } from 'react-redux';
import { LoginFailModal } from './LoginFailModal';
import './Login.css';
import { clearFailedMsg } from './auth/actions';
import { Link } from 'react-router-dom';
import { Container, Button, Label, Input, Form } from 'reactstrap';


interface ILoginProps {
    login: (username: string, password: string) => void,
    msg: string,
    clearFailedMsg: () => void
}

interface ILoginState {
    username: string,
    password: string
    showModal: boolean,
}

class Login extends React.Component<ILoginProps, ILoginState>{

    constructor(props: ILoginProps) {
        super(props);
        //  this.state = { value: '' };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            username: "",
            password: "",
            showModal: false,
        }
    }

    private onChange = (field: 'username' | 'password', e: React.FormEvent<HTMLInputElement>) => {
        const state = {};
        state[field] = e.currentTarget.value;
        this.setState(state);
    }

    private handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        // alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
        const a = console;
        const { username, password } = this.state;
        a.log(username, password);
        if (username && password) {
            this.props.login(username, password);
        }
    }

    // private login = () => {
    //     const { username, password } = this.state;
    //     if (username && password) {
    //         this.props.login(username, password);
    //     }
    // }



    public render() {
        return (
            <div id="loginPage">
                <Container>
                    <img id="pic" src="https://s3-ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/logo.png" />
                    <br />
                    <Form onSubmit={this.handleSubmit}>
                        <Label>用戶名稱</Label>
                        <Input type="text"
                            value={this.state.username}
                            onChange={this.onChange.bind(this, 'username')}
                            placeholder="輸入用戶名稱" />
                        <br />
                        <Label>密碼</Label>
                        <Input type="password"
                            value={this.state.password}
                            onChange={this.onChange.bind(this, 'password')}
                            placeholder="輸入密碼" />
                        <br />
                        <br />
                        <Button>提交</Button>
                        <Link to="/">
                            <Button>返回首頁</Button>
                        </Link>
                    </Form>
                    {
                        this.props.msg && <LoginFailModal
                            msg={this.props.msg}
                            close={this.props.clearFailedMsg}
                        />
                    }
                </Container>

            </div>
        );
    }
}


const mapStateToProps = (state: IRootState) => ({
    msg: state.auth.msg
});

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        login: (username: string, password: string) => dispatch(login(username, password)),
        clearFailedMsg: () => dispatch(clearFailedMsg())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)