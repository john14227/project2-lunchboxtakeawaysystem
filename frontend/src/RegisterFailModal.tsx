import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
// import { Link } from 'react-router-dom';


interface IRegisterFailModalProps {
    // open: boolean;
    msg: string;
    close: () => void
}


export function RegisterFailModal(props: IRegisterFailModalProps) {
    return (
        <Modal isOpen={true}>

            <ModalHeader> Register </ModalHeader>
            <ModalBody>
                {props.msg}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={props.close}>Close</Button>
                {/* </Link> */}

            </ModalFooter>


        </Modal>
    )
}

