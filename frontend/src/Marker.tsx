import * as React from 'react';


interface IMarkerProps{
    lat:number
    lng:number
    name:string
    address:string
}

interface IMarkerState{
    isInfoShown:boolean
}

interface IInfoBox{
    address:string
}

function InfoBox(props:IInfoBox){
    return (
    <div style={{
            color: 'white',
            background: 'grey',
            padding: '15px 10px',
            display: 'inline-flex',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: '100%',
            transform: 'translate(0, 50%)'
        }}>
        {props.address}
    </div>
    )
}

export default class Marker extends React.Component<IMarkerProps,IMarkerState>{
    constructor(props:any){
        super(props)
        this.state={
            isInfoShown:false
        }
    }

    private showBox = () => {
        this.setState({
            isInfoShown:!this.state.isInfoShown
        })
    }

    public render(){
        return(
            <div onClick={this.showBox} style={{
            color: 'white',
            background: 'grey',
            padding: '15px 10px',
            display: 'inline-flex',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: '50px',
            transform: 'translate(-50%, -50%)'
            }} 
                >
                {/* {this.props.name} */}
                {this.state.isInfoShown? <InfoBox 
                                        address={this.props.address}
                                        /> :""}
            </div>
        );
    }
}