import * as React from 'react';
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption } from 'reactstrap';

const items = [
    {
        src: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557235230348.jpeg',
        altText: '',
        caption: ''
    },
    {
        src: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557235079140.jpeg',
        altText: '',
        caption: ''
    },
    {
        src: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557235299298.jpeg',
        altText: '',
        caption: ''
    }

];

// interface ITeachProps {

// }

interface ITeachState {
    activeIndex: number;


}

class Teach extends React.Component<{}, ITeachState> {

    private animating: any;

    constructor(props: any) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);


    };


    private onExiting() {
        this.animating = true;
    }

    private onExited() {
        this.animating = false;
    }

    private next() {
        if (this.animating) {
            return;
        }
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    private previous() {
        if (this.animating) { return };
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    private goToIndex(newIndex: number) {
        if (this.animating) { return };
        this.setState({ activeIndex: newIndex });
    }

    public render() {
        const { activeIndex } = this.state;

        const slides = items.map((item) => {
            return (
                <CarouselItem
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >

                    <img className='teach' src={item.src} alt={item.altText} />
                    <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
                </CarouselItem>
            );
        });

        return (
            <Carousel
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
            >
                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
            </Carousel>
        );
    }
}



export default Teach;