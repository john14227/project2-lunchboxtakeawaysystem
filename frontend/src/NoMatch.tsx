import * as React from 'react';

export function NoMatch(props: {}) {
    return (<div>
        Looks like you are accessing a wrong page!
    </div>
    )
}