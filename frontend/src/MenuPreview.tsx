import * as React from 'react'
import { loadMenu } from './home/thunks';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { MenuItemPreview } from './MenuItemPreview';
import { match } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';

interface IMenuPreviewProps {
    match: match<{ id: string }>
    history: {
        push: (url: string) => void
    }
    menu: any[]
    menuMsg: string
    loadMenu: () => void
}

class MenuPreview extends React.Component<IMenuPreviewProps>{
    constructor(props: any) {
        super(props)
    }


    private goToMenuItem = (id: number) => {
        this.props.history.push(`/menu/${id}`);
    }

    private goToMoreOptions = () => {
        this.props.history.push('/menu');
    }

    private menuPreview = () => {
        const preview = [];
        let menuLength = this.props.menu.length;
        menuLength >= 4 ? menuLength = 4 : menuLength = this.props.menu.length;
        for (let i = 0; i < menuLength; i++) {
            const item = this.props.menu[i];
            preview.push(
                <Col sm="3" key={`${item.id}`} className='menu-item' onClick={this.goToMenuItem.bind(this, item.id)}>
                    <MenuItemPreview item={item} />
                </Col>
            );
        }
        return preview;
    }

    public render() {
        return (
            <div id='menu-div'>

                <Container>
                    <h1>今日精選：</h1>
                    <Row id="Menu" className="menu-flex">

                        {/* {this.props.menu.map(item => (
                            <Col sm="3" key={`${item.id}`} className='menu-item' onClick={this.goToMenuItem.bind(this, item.id)}>
                                <MenuItemPreview item={item} />
                            </Col>
                        ))} */}
                        {this.menuPreview()}


                    </Row>
                    <div id='more-options' onClick={this.goToMoreOptions}>
                        <a>查看更多選擇</a>
                    </div>
                </Container>
            </div >
        )
    }

}

const mapStateToProps = (state: IRootState) => {
    const { menu, menuMsg } = state.home
    return {
        menu,
        menuMsg,
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadMenu: () => dispatch(loadMenu())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuPreview)
