import * as React from 'react';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import { update } from './user/thunks';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';


interface IUserProfileProps {
    msg: string
    clearFailedMsg: () => void
    user:{}
    update:(user:any)=>void

}

interface IUserProfileState {
    password: string
    email: string
    telephone: string
    contact_person: string
    credit_card_num: string
}

class UserProfile extends React.Component<IUserProfileProps,IUserProfileState>{
    constructor(props:any){
        super(props);
        this.state = {
            password: '',
            email: '',
            telephone: '',
            contact_person: '',
            credit_card_num: ''
        }
    }

    private handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
    }

    private update = async() =>{
        const { password, email, telephone, contact_person, credit_card_num } = this.state;
        if ( password && email && telephone && contact_person && credit_card_num) {
            const user = {
                password,
                email,
                telephone,
                contact_person,
                credit_card_num
            }
            await this.props.update(user);
        }
    }

    private onChange = (field: 'password | email | telephone | contact_person | credit_card_num', e: React.FormEvent<HTMLInputElement>) => {
        const state = {};
        state[field] = e.currentTarget.value;
        this.setState(state);
    }

    public render(){
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for='password'>密碼</Label>
                        <Input type='password' name='password' id='username' placeholder='密碼' onChange={this.onChange.bind(this, 'password')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='email'>電郵地址</Label>
                        <Input type='email' name='email' id='email' placeholder='電郵地址' onChange={this.onChange.bind(this, 'email')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='telephone'>聯絡電話號碼</Label>
                        <Input type='text' name='telephone' id='telephone' placeholder='聯絡電話號碼' onChange={this.onChange.bind(this, 'telephone')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='contact_person'>聯絡人姓名</Label>
                        <Input type='text' name='telephone' id='contact_person' placeholder='聯絡人姓名' onChange={this.onChange.bind(this, 'contact_person')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='credit_card_num'>信用卡號碼</Label>
                        <Input type='text' name='credit_card_num' id='credit_card_num' placeholder='信用卡號碼' onChange={this.onChange.bind(this, 'credit_card_num')} />
                    </FormGroup>
                    <Button form='register-form' color="secondary" type='submit' onClick={this.update} name='submit' id='submit'>提交</Button>&nbsp;&nbsp;&nbsp;&nbsp;
                </Form>

            </div>
        )
    }
}

const mapStateToProps = (state:IRootState) => {
    const {user,msg} = state.user
    return {
        user,
        msg
    }
}

const mapDispatchToProps = (dispatch:ThunkDispatch) => {
    return {
        update:(user:any)=>dispatch(update(user))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(UserProfile);