import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Table } from 'reactstrap';
import { Dispatch } from 'redux';
import { addPurchaseItemSuccess } from './home/actions';
import { connect } from 'react-redux';
import { IRootState } from './store';
// import { Link } from 'react-router-dom';

interface IShoppingItemState{
    id:number
    food:string
    quantity:number
    price:number
}

interface IItemPurchaseModalProps {
    // open: boolean;
    addPurchaseItemSuccess: (purchaseItem:IShoppingItemState) => void
    close: () => void
    item: {
        id: number
        food: string
        img_url: string
        order_deadline: string
        kol_intro_video: string
        kol_fb_link: string
        kol_ig_link: string
        price: number
    }
    purchaseItems:any[]
}

interface IItemPurchaseModalState {
    quantity: number
}

class ItemPurchaseModal extends React.Component<IItemPurchaseModalProps, IItemPurchaseModalState >{
    constructor(props: IItemPurchaseModalProps) {
        super(props);
        this.state = {
            quantity: 1
        }
    }

    private calculateTotalAmount = () => (
        this.props.item.price * this.state.quantity
    )

    private addItemNumber = () =>{
        this.setState({
            quantity: this.state.quantity+1
        })
    }

    private substractItemNumber = ()=>{
        if (this.state.quantity <= 1){
            this.setState({
                quantity: 1
            })
        } else {
            this.setState({
                quantity: this.state.quantity-1
            })
        }
    }

    private addToShoppingList = () => {
        const shoppingItem = {
            id:this.props.item.id,
            food:this.props.item.food,
            quantity:this.state.quantity,
            price:(this.state.quantity)*(this.props.item.price)
        }

        this.props.addPurchaseItemSuccess(shoppingItem);
        this.props.close();
    }


    public render() {
        return (
            <Modal isOpen={true}>
                <ModalHeader>購買項目</ModalHeader>
                <ModalBody>
                    <Table>
                        <thead>
                            <tr>
                                <th>貨品名稱</th>
                                <th>數量</th>
                                <th>價錢</th>
                            </tr>

                        </thead>
                        <tbody>
                            <tr>
                                <th>{this.props.item.food}</th>
                                <th><Button onClick={this.addItemNumber}>+</Button>{this.state.quantity}<Button onClick={this.substractItemNumber}>-</Button></th>
                                <th>{this.calculateTotalAmount()}</th>
                            </tr>
                        </tbody>

                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={this.addToShoppingList}>確認</Button>
                    <Button color="primary" onClick={this.props.close}>關閉</Button>
                    {/* </Link> */}

                </ModalFooter>


            </Modal>
        )
    }

}

const mapStateToProps = (state:IRootState) => {
    const {purchaseItems} = state.home
    return {
        purchaseItems
    }
}



const mapDispatchToProps = (dispatch:Dispatch) => {
    return {
        addPurchaseItemSuccess: (purchaseItem:IShoppingItemState) => dispatch(addPurchaseItemSuccess(purchaseItem))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(ItemPurchaseModal);
