import * as React from 'react';
import { CardElement } from 'react-stripe-elements';
import { Label } from 'reactstrap';


const handleBlur = () => {
    console.log('[blur]');
};

const handleChange = (change: any) => {
    console.log('[change]', change);
};

const handleFocus = () => {
    console.log('[focus]');
};
const handleReady = () => {
    console.log('[ready]');
};

interface ICardFormProps {
    fontSize: any;
}

const createOptions = (fontSize: any, padding: any) => {
    return {
        style: {
            base: {
                fontSize,
                color: '#424770',
                letterSpacing: '0.025em',
                fontFamily: 'Source Code Pro, monospace',
                '::placeholder': {
                    color: '#aab7c4',
                },
                padding,
            },
            invalid: {
                color: '#9e2146',
            },
        },
    };
};


export class CardForm extends React.Component<ICardFormProps, {}> {



    public render() {


        return (
            <Label>
                Card details
                            <CardElement
                    onBlur={handleBlur}
                    onChange={handleChange}
                    onFocus={handleFocus}
                    onReady={handleReady}
                    hidePostalCode={true}
                    {...createOptions(this.props.fontSize, 0)}
                />
            </Label>
        )

    }
}
