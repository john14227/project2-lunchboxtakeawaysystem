import { IAuthActions } from './actions';

const initialState = {
    isAuthenticated: localStorage.getItem('token') != null,
    msg: ""
}
export function authReducer(state = initialState, action: IAuthActions) {
    switch (action.type) {
        case 'CLEAR_FAILED_MSG':
            return { ...state, msg: '' }
        case 'LOGIN_SUCCESS':
            return { ...state, isAuthenticated: true };
        case 'LOGIN_FAILED':
            return { ...state, isAuthenticated: false, msg: action.msg }
        case 'LOGOUT_SUCCESS':
            return { ...state, isAuthenticated: false }
        case 'REGISTER_SUCCESS':
            return { ...state, msg: action.msg };
        case 'UPDATE_SUCCESS':
            return state;
        case 'UPDATE_FAILED':
            return { ...state, msg: action.msg };
        default:
            return state;
    }
}

