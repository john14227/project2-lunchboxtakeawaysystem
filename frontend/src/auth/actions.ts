export type loginSuccess = 'LOGIN_SUCCESS';
export type loginFailed = 'LOGIN_FALIED';
export type logoutSuccess = 'LOGOUT_SUCCESS';
export type clearFailedMsg = "CLEAR_FAILED_MSG";

export function loginSuccess() { // :{type:Success}
    return {
        type: 'LOGIN_SUCCESS' as 'LOGIN_SUCCESS'
    }
}

export function loginFailed(msg: string) {
    return {
        type: 'LOGIN_FAILED' as 'LOGIN_FAILED',
        msg
    }
}

export function UpdateSuccess() { // :{type:Success}
    return {
        type: 'UPDATE_SUCCESS' as 'UPDATE_SUCCESS'
    }
}

export function UpdateFailed(msg: string) {
    return {
        type: 'UPDATE_FAILED' as 'UPDATE_FAILED',
        msg
    }
}



export function clearFailedMsg() {
    return {
        type: "CLEAR_FAILED_MSG" as "CLEAR_FAILED_MSG"
    }
}

export function logoutSuccess() {
    return {
        type: 'LOGOUT_SUCCESS' as 'LOGOUT_SUCCESS'
    }
}

export function registerSuccess(msg: string) {
    return {
        type: 'REGISTER_SUCCESS' as 'REGISTER_SUCCESS',
        msg

    }
}

export function registerFailed(msg: string) {
    return {
        type: 'REGISTER_FAILED' as 'REGISTER_FAILED',
        msg
    }
}

export type IAuthActions = ReturnType<typeof loginSuccess |
    typeof loginFailed |
    typeof logoutSuccess |
    typeof clearFailedMsg |
    typeof registerSuccess |
    typeof registerFailed |
    typeof UpdateSuccess |
    typeof UpdateFailed>;

// export interface IAuthState {
//     type: loginSuccess | loginFailed
// }

// let myname = "Hello" as "Hello";
// myname = "Hellos"