import { IAuthActions, loginSuccess, loginFailed, logoutSuccess, registerSuccess, registerFailed } from './actions';
import { Dispatch } from 'redux';
import { CallHistoryMethodAction, push } from 'connected-react-router';

const REACT_APP_API_SERVER = process.env.REACT_APP_API_SERVER;


export function login(username: string, password: string) {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ username, password })
        });
        const result = await res.json();
        if (res.status === 200) {
            dispatch(loginSuccess());
            localStorage.setItem('token', result.token);
            dispatch(push("/"));
        } else {
            dispatch(loginFailed(result.msg));
        }
    }
}


export function logout() {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        localStorage.removeItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/logout`);
        await res.json();
        dispatch(logoutSuccess());
        dispatch(push('/'))
    }

}

export function register(user: any) {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        console.log(user);
        const res = await fetch(`${REACT_APP_API_SERVER}/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        });
        const result = await res.json();
        if (res.status === 200) {
            dispatch(registerSuccess(result.msg));
        } else {
            dispatch(registerFailed(result.msg));
        }
    }
}


