import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
// import { Link } from 'react-router-dom';


interface ILoginFailModalProps {
    // open: boolean;
    msg: string;
    close: () => void
}


export function LoginFailModal(props: ILoginFailModalProps) {
    return (
        <Modal isOpen={true}>

            <ModalHeader> Login </ModalHeader>
            <ModalBody>
                Error!{props.msg}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={props.close}>Close</Button>
                {/* </Link> */}

            </ModalFooter>


        </Modal>
    )
}

