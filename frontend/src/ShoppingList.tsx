import * as React from 'react';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from './store';
import { removePurchaseItemSuccess, shoppingListReminder, clearShoppingListReminder } from './home/actions';
import { Table, Button, Input } from 'reactstrap';
import { submitOrder } from './user/thunks';
import { ShoppingListReminder } from './ShoppingListReminder';


interface IShoppingItemState {
    id: number
    food: string
    quantity: number
    price: number
}

interface IShoppingListProps {
    purchaseItems: any[]
    locationId: number
    shoppingListMsg:string
    removePurchaseItemSuccess: (purchaseItems: any) => void
    shoppingListReminder:(shoppingListMsg:string) =>void
    submitOrder: (order:any)=>void
    clearShoppingListReminder:()=>void
}

interface IShoppingListState {
    purchaseItems: any[]
    totalAmount: number
    specialRequirement: string
}

class ShoppingList extends React.Component<IShoppingListProps, IShoppingListState>{
    constructor(props: any) {
        super(props)
        this.state = {
            purchaseItems: [],
            totalAmount: 0,
            specialRequirement: ''
        }
    }


    private onSubmit = async (event: any) => {
        event.preventDefault();
        const order = {
            menuItems: this.state.purchaseItems,
            bill_amount: this.state.totalAmount,
            special_requirement: this.state.specialRequirement
        } as any;

        if (this.props.locationId > 0) {
            order.pick_up_point_id = this.props.locationId
            await this.props.submitOrder(order);
        } else {
            const msg = '請選擇自取點'
            this.props.shoppingListReminder(msg);
        }
    }

    private handleChange = (field: 'specialRequirement', e: React.FormEvent<HTMLInputElement>) => {
        const state = {}
        state[field] = e.currentTarget.value;
        this.setState({ specialRequirement: state[field] });
    }

    private removeItems = (i: number) => {
        // any
        const purchaseItems = this.state.purchaseItems.concat();
        purchaseItems.splice(i, 1);
        let totalAmount = 0;
        for (const item of purchaseItems) {
            totalAmount += item.price
        }

        this.setState({
            purchaseItems,
            totalAmount
        }, () => {
            this.props.removePurchaseItemSuccess(purchaseItems);
        })
    }

    // public componentDidUpdate(){
    //     let totalAmount = 0;
    //     for (const item of this.state.purchaseItems) {
    //         totalAmount += item.price
    //     }

    //     this.setState({
    //         totalAmount
    //     });
    // }

    public componentDidMount() {
        let totalAmount = 0;
        for (const item of this.state.purchaseItems) {
            totalAmount += item.price
        }

        const a = console;
        a.log(totalAmount);
        this.setState({
            totalAmount
        });
    }
    

    public componentWillMount() {
        let totalAmount = 0;
        for (const item of this.state.purchaseItems) {
            totalAmount += item.price
        }
        
        this.setState({
            purchaseItems: this.props.purchaseItems,
            totalAmount
        });
    }

    public render() {
        return (
            <div>
                {(this.state.purchaseItems).length > 0 ?
                    <Table>
                        <thead>
                            <tr>
                                <th>貨品名稱</th>
                                <th>數量</th>
                                <th>價錢</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.purchaseItems.map((item: IShoppingItemState, i: number) => (
                                <tr>
                                    <th>{item.food}</th>
                                    <th>{item.quantity}</th>
                                    <th>{item.price}</th>
                                    <th><Button onClick={this.removeItems.bind(this, i)}>移除</Button></th>
                                </tr>
                            ))}
                        </tbody>
                        <tfoot>
                                <div>
                                    總金額: {this.state.totalAmount}
                                </div>
                                <div><Button onClick={this.onSubmit}>付款</Button></div>
                                <div>
                                    特別要求:
                                    <Input type='textarea' name='specialRequirement' id='specialRequirement' onChange={this.handleChange.bind(this, 'specialRequirement')} placeholder='請寫下各菜式特別要求(如有)' />
                                </div>

                        </tfoot>
                    </Table>
                    : <span>你仲未叫嘢食喎!</span>}
                    {this.props.shoppingListMsg && <ShoppingListReminder 
                                                    close={this.props.clearShoppingListReminder}
                                                    msg={this.props.shoppingListMsg}
                                                        />}

            </div>
        )
    }
}


const mapStateToProps = (state: IRootState) => {
    const { locationId, purchaseItems,shoppingListMsg } = state.home

    return {
        locationId,
        purchaseItems,
        shoppingListMsg
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        removePurchaseItemSuccess: (purchaseItems: any[]) => dispatch(removePurchaseItemSuccess(purchaseItems)),
        submitOrder: (order:any)=>dispatch(submitOrder(order)),
        shoppingListReminder : (shoppingListMsg:string) => (dispatch(shoppingListReminder(shoppingListMsg))),
        clearShoppingListReminder:()=>(dispatch(clearShoppingListReminder()))
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingList);