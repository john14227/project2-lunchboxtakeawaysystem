import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
// import { Link } from 'react-router-dom';


interface IRegisterSuccessModalProps {
    // open: boolean;
    msg: string;
    close: () => void

}


export function RegisterSuccessModal(props: IRegisterSuccessModalProps) {
    return (
        <Modal isOpen={true}>

            <ModalHeader> Register </ModalHeader>
            <ModalBody>
                {props.msg}
            </ModalBody>
            <ModalFooter>
                <Link to="/">
                    <Button color="primary" onClick={props.close}>Back to Home</Button></Link>
                <Link to="/login">
                    <Button color="success" onClick={props.close}>立即登入</Button></Link>


            </ModalFooter>


        </Modal >
    )
}

