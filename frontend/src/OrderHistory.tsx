import * as React from 'react'
import { ThunkDispatch, IRootState } from 'src/store';
import { loadUserOrderHistory } from 'src/user/thunks';
import { connect } from 'react-redux';
import { Table, Col, Container } from 'reactstrap';



interface IOrderHistoryProps {
    history: {
        push: (url: string) => void
    }
    loadUserOrderHistory: () => void
    userOrderHistory: any[]
}


// interface IOrderHistoryState {
//     userOrderHistory: any[]
// }

class OrderHistory extends React.Component<IOrderHistoryProps>{
    constructor(props: any) {
        super(props)
        // this.state = {
        //     userOrderHistory: []
        // }
    }

    public async componentWillMount() {
        await this.props.loadUserOrderHistory();

    }

    public render() {
        return (
            <div>
                <Container>
                    <Col sm='12'>
                        <Table bordered={true} responsive={true}>
                            <thead>
                                <tr>
                                    <th>訂單編號</th>
                                    <th>點選項目</th>
                                    <th>落單日期</th>
                                    <th>落單時間</th>
                                    <th>總金額(HKD)</th>
                                </tr>

                            </thead>

                            <tbody>
                                {this.props.userOrderHistory.map((orderHistory: any) => {
                                    return <tr key={orderHistory.id}>
                                        <th>{orderHistory.tracking_number}</th>
                                        <th>{orderHistory.menuItems.map((item: any) => (
                                            <div>
                                                {item.food} (${item.price}) (數量: {item.quantity})
                                                <br />
                                            </div>
                                        ))
                                        }</th>
                                        <th>{orderHistory.createdDate}</th>
                                        <th>{orderHistory.createdTime}</th>
                                        <th>{orderHistory.bill_amount}</th>
                                    </tr>
                                })}
                            </tbody>

                        </Table>
                    </Col>
                </Container>


            </div>

        )
    }
}



// export function Myorders(props: {}) {
//     return (
//         <div>
//             Myorders Page
//         </div>
//     )
// }

const mapStateToProps = (state: IRootState) => {
    const userOrderHistory = state.user.userOrderHistory
    return {
        userOrderHistory
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadUserOrderHistory: () => (dispatch(loadUserOrderHistory()))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);