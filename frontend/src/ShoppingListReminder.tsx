import * as React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
// import { Link } from 'react-router-dom';


interface ILoginFailModalProps {
    // open: boolean;
    close: () => void
    msg:string
}


export class ShoppingListReminder extends React.Component< ILoginFailModalProps> {


    public render (){
        return (
            <Modal isOpen={true}>
    
                <ModalHeader> 提示 </ModalHeader>
                <ModalBody>
                    {this.props.msg}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.props.close}>Close</Button>
                    {/* </Link> */}
    
                </ModalFooter>
    
            </Modal>
        )
    }
}

