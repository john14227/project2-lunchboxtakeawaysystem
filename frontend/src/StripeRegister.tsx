import * as React from 'react';
import Register from './Register';
import { Elements } from 'react-stripe-elements';
import { ThunkDispatch, IRootState } from './store';
import { connect } from 'react-redux';
import { clearFailedMsg } from './auth/actions';
import { register } from './auth/thunks'

interface IStripeRegisterProps {
    register: (user: any) => void
    msg: string
    clearFailedMsg: () => void
}

class StripeRegister extends React.Component<IStripeRegisterProps> {
    public render() {
        return (
            <Elements>
                <Register
                    register={this.props.register}
                    clearFailedMsg={this.props.clearFailedMsg}
                    msg={this.props.msg}
                />

            </Elements>
        )
    }
}


const mapStateToProps = (state: IRootState) => {
    return {
        msg: state.auth.msg
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        register: (user: any) => dispatch(register(user)),
        clearFailedMsg: () => dispatch(clearFailedMsg())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StripeRegister)