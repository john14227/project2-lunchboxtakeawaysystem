import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from './Login';
import Home from './Home'
import { Provider } from 'react-redux';
import store from './store'
import { history } from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch } from 'react-router';
import { NoMatch } from './NoMatch';
// import Register from './Register';
import { StripeProvider } from 'react-stripe-elements';
import StripeRegister  from './StripeRegister';
// import Register from './Register';
// import Menu from './Menu';
library.add(fab);
class App extends React.Component {
  public render() {
    return (
      <div>
        {/* <span className="App-title">卅二公館 外賣自取訂購平台</span> */}
       <StripeProvider apiKey="	
pk_test_vOO7S6lWWYY0jyZ1ZiWhUOsl006BA4isp2">
        <Provider store={store}>
          
            <ConnectedRouter history={history}>
              <Switch>
                <Route path='/login' component={Login} />
                <Route path='/register' component={StripeRegister} />
                <Route path="/" component={Home} />
                {/* <Route path={'/menu/:id'} /> */}
                {/* <Route path='/' exact={true} component={Home} />

                  <Route path='/login' component={Login} /> */}
                {/* <Route path='/admin/login' component={AdminLogin} /> */}
                <Route component={NoMatch} />
                {/* <privateRoute path='/user' component={User} /> */}
              </Switch>
            </ConnectedRouter>
          
        </Provider>
        </StripeProvider>
        {/* <div className="App">
          <header className="App-header">
           
          </header>

        </div> */}
      </div >
    );
  }
}

export default App;
