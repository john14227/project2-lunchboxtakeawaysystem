import * as React from 'react';
import { match, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IRootState } from './store'
import { loadMenu, loadPromotion, loadAllLocation } from './home/thunks';
import UserDropdownMenu from './UserDropdownMenu'
import './Home.css'
import logo_color from './logo_color.png';
// import food_ad_banner from './food_ad_banner.jpg';

import downloadLogo from './downloadLogo.svg'
import { ThunkDispatch } from './store';
import Menu from './Menu';
import { menuItemDescription } from './MenuItemDescription';
import MenuPreview from './MenuPreview';
import Map from './Map';
// import { clearSearchResult } from './home/actions';
import { logout } from './auth/thunks';
import { PrivateRoute } from './PrivateRoute';
import OrderHistory from './OrderHistory';
import Search from './Search';
import { Container, Row, Col } from 'reactstrap';
import UserProfile from './UserProfile';
import Teach from './Teach';
import ShoppingList from './ShoppingList';





interface IHomeProps {
    match: match<{ id: string }>
    history: {
        push: (url: string) => void
    }
    menu: any[]
    promotion: any[]
    menuMsg: string
    promotionMsg: string
    // searchResult: string[]
    allLocation: string[]
    locationMsg: string
    loadMenu: () => void
    loadPromotion: () => void
    // loadLocation: (nameOrAddress: string) => void
    loadAllLocation: () => void
    // clearSearchResult: () => void
    logout: () => void
}

interface IHomeState {
    display: boolean
    searchTarget: string
}



class Home extends React.Component<IHomeProps, IHomeState>{

    constructor(props: IHomeProps) {
        super(props);
        // this.goToLoginPage.bind(this);
        this.state = {
            searchTarget: '',
            display: true
        };
    }

    private goToHomePage = () => {
        this.props.history.push('/');
    }

    private goToLoginPage = () => {
        // location.href="/login"
        this.props.history.push('/login');
    }


    private goToRegisterPage = () => {
        // location.href="/register"
        this.props.history.push('/register');
    }

    // private goToMenuItem = (id: number) => {
    //     this.props.history.push(`/menu/${id}`);
    // }

    // private goToMoreOptions = () => {
    //     this.props.history.push('/menu');
    // }
    // private mapSearchResult = () => {

    // }

    private showUserComponent() {
        if (localStorage.getItem('token')) {
            return (
                <div>
                        
                    <UserDropdownMenu
                        logout={this.props.logout}
                        history={this.props.history}
                    />
                </div>
            )
        } else {
            return (
                <div id='auth '>
                    <button className="button" onClick={this.goToRegisterPage}>註冊</button>
                    <button className="button" onClick={this.goToLoginPage}>登入</button>
                </div>

            )
        }
    }

    // private renderMenuPreview (){
    //     return <Menu match={this.props.match}
    //                 history={this.props.history}
    //                 menu={this.props.menu}
    //                 menuMsg={this.props.menuMsg}
    //                 loadMenu={this.props.loadMenu}/>
    // }

    public async componentWillMount() {
        await this.props.loadMenu();
        await this.props.loadPromotion();
        // await this.props.loadAllLocation();
    }

    // private handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    //     event.preventDefault();
    // }

    // private searchLocation = async() => {
    //     const searchTarget = this.state.searchTarget;
    //     // await this.props.loadLocation(searchTarget);
    //     if (searchTarget.length > 1 && /[\S]/g.test(searchTarget)){
    //         await this.props.loadLocation(searchTarget);
    //     } 
    // }

    // private onChange = (field: 'searchTarget', e: React.FormEvent<HTMLInputElement>) => {
    //     const state = {};
    //     const a = console;
    //     state[field] = e.currentTarget.value;
    //     this.setState(state,()=>{
    //         this.props.clearSearchResult();
    //         this.searchLocation();
    //     });
    //     a.log(state);

    // }

    // private onInputValueChange = async (inputValue: string, stateAndHelpers: ControllerStateAndHelpers<object>) => {
    //     await this.props.loadLocation(inputValue);
    // }

    public render() {
        // const Img = this.loadImg();
        const UserComponent = this.showUserComponent();
        return (
            <div>
                <div id='common-share-layer'>
                    <Container>
                        <Row>
                            <Col ms='6' id="logo">
                                <img onClick={this.goToHomePage} src={logo_color} className="App-logo" alt="logo" />
                            </Col>


                            <Col ms='6' id='user-component'>
                                {UserComponent}
                            </Col>

                        </Row>
                    </Container>

                </div>

                <div id='search-bar-div'>
                    <Container>
                        <Row >
                            <Col ms='12' id='search-padding'>
                                {/* <Form id='search-location' onSubmit={this.handleSubmit}>
                            <Input type='text' name='searchTarget' id = 'searchTarget' placeholder='輸入自取點名稱或地址' onChange={this.onChange.bind(this,'searchTarget')} />
                            <Button form='search-location' >搜尋</Button>
                            </Form> */}
                                {/* <SearchBar /> */}
                                <div id='search-bar-font'>
                                    選擇最方便的自取點，在地圖上一目了然
                                    </div><br /><br />
                                <div id='search-bar'><Search /></div>
                            </Col>

                        </Row>
                    </Container>
                </div>
                <div id='map-div'>

                    <Col sm='12' id='map'>
                            

                        <Route path='/' exact={true} component={Map} />
                    </Col>


                </div>

                {/* <Route path='/' exact={true} compoenent={Map} /> */}


                <Route path='/' exact={true} component={Teach} />
                <div id='content'>
                    {/* <div id='map' style={{ height: '50vh', width: '100vh' }}>
                            <Map
                                loadAllLocation={this.props.loadAllLocation}
                                allLocation={this.props.allLocation}
                            />
                        </div> */}


                    <div id='sections'>
                        {/* <div>Promotion
                            {this.props.promotion.map(item => (
                            <div>{item.url}</div>
                        ))}

                    </div>

                    {/* <div>
                     
                        <div id = "Menu">
                            {this.props.menu.map(item => (
                                    <div key = {`${item.id}`} className="menu-item" onClick={this.goToMenuItem.bind(this,item.id)}>
                                    <MenuItemPreview item = {item} />
                                </div>
                            ))}
                            <div id = 'more-options'  onClick = {this.goToMoreOptions}>
                                查看更多選擇
                            </div>
                        </div>        
                    </div> */}
                        <Route path='/' exact={true} component={MenuPreview} />
                        <Route path='/menu' exact={true} component={Menu} />
                        <Route path={`/menu/:id`} exact={true} component={menuItemDescription} />
                        <PrivateRoute path='/orderhistory' exact={true} component={OrderHistory} />
                        <PrivateRoute path='/profile' exact={true} component={UserProfile} />
                        <PrivateRoute path='/purchase' exact={true} component={ShoppingList} />


                    </div>



                    {/* </div>

                </div > */}


                    <div id='footer'>
                        <div id='footer-inner'>

                            <Container>
                                <Row id='footer-row'>

                                    <Col ms='3' id='footer-col'>
                                        <h5>關於卅二公館</h5>
                                        <div>媒體報導</div>
                                        <div>技術日誌</div>
                                        <div>工作機會</div>
                                        <div>餐廳加盟</div>
                                    </Col>
                                    <Col ms='3' id='footer-col'>
                                        <h5>法律</h5>
                                        <div>使用條款</div>
                                        <div>私隱政策及條款</div>
                                        <div>Cookie信息</div>
                                    </Col>
                                    <Col ms='3' id='footer-col'>
                                        <h5>幫助</h5>
                                        <div>聯絡方法</div>
                                        <div>常見問題</div>
                                        <div>網站地圖</div>
                                    </Col>
                                    <Col ms='3' id='footer-col'>
                                        <h5>下載</h5>
                                        <br />
                                        <img src={downloadLogo} alt="downloadLogo" />
                                    </Col>
                                </Row>
                            </Container>
                            <Container id='icon-bar'>
                                <Row>
                                    <FontAwesomeIcon icon={['fab', 'facebook-square']} size={"3x"} />
                                    <FontAwesomeIcon icon={['fab', 'twitter-square']} size={"3x"} />
                                    <FontAwesomeIcon icon={['fab', 'instagram']} size={"3x"} />
                                </Row>
                            </Container>
                        </div>


                    </div>
                </div >


            </div >

        )
    }
}

const mapStateToProps = (state: IRootState) => {
    const { menu, promotion, menuMsg, promotionMsg, searchResult, allLocation, locationMsg } = state.home
    return {
        menu,
        promotion,
        menuMsg,
        promotionMsg,
        searchResult,
        allLocation,
        locationMsg
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadPromotion: () => dispatch(loadPromotion()),
        loadMenu: () => dispatch(loadMenu()),
        // loadLocation: (nameOrAddress: string) => dispatch(loadLocation(nameOrAddress)),
        loadAllLocation: () => dispatch(loadAllLocation()),
        // clearSearchResult: () => dispatch(clearSearchResult()),
        logout: () => dispatch(logout()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)

