export function authorizedFetch(input: RequestInfo, init?: RequestInit) {
    return fetch(input, {
        ...init,
        headers: {
            ...(init ? init.headers : {}),
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
}