import * as React from 'react'
// import { register } from './auth/thunks'
// import { connect } from 'react-redux';
// import { ThunkDispatch, IRootState } from './store';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { RegisterFailModal } from './RegisterFailModal'
import { RegisterSuccessModal } from './RegisterSuccessModal'
import { Link } from 'react-router-dom';
// import { clearFailedMsg } from './auth/actions';
import './Register.css';
import { injectStripe } from 'react-stripe-elements';
import { CardForm } from './Payment';

interface IRegisterProps {
    register: (user: any) => void
    msg: string
    clearFailedMsg: () => void
    stripe?: any
}

interface IRegisterState {
    username: string
    password: string
    email: string
    telephone: string
    contact_person: string

}




class Register extends React.Component<IRegisterProps, IRegisterState>{

    constructor(props: IRegisterProps) {
        super(props);
        this.state = {
            username: '',
            password: '',
            email: '',
            telephone: '',
            contact_person: '',

        }
    }

    private onChange = (field: 'username' | 'password | email | telephone | contact_person ', e: React.FormEvent<HTMLInputElement>) => {
        const state = {};
        state[field] = e.currentTarget.value;
        this.setState(state);
    }

    private handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (this.props.stripe) {
            this.props.stripe
                .createToken()
                .then((payload: any) => {
                    const token = payload.token.id;
                    console.log('[token]', token)
                    this.register(token)
                });
        } else {
            console.log("Stripe.js hasn't loaded yet.");
        }
    };


    private register = async (token: any) => {
        console.log('registertsx', this.state);
        console.log('registertsx-token', token);
        const { username, password, email, telephone, contact_person } = this.state;

        if (username && password && email && telephone && contact_person) {
            const user = {
                username,
                password,
                email,
                telephone,
                contact_person,
                credit_card_num: token + ''

            }
            // user = { ...user, token};
            console.log('register', user)
            await this.props.register(user);
        }
    }

    public render() {
        return (
            <div id="registerPage">

                <img id="pic" src="https://s3-ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/logo.png" />

                <p>請輸入個人資料</p>

                <Form id='register-form' onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for='username'>用戶名稱</Label>
                        <Input type='text' name='username' id='username' placeholder='用戶名稱' onChange={this.onChange.bind(this, 'username')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='password'>密碼</Label>
                        <Input type='password' name='password' id='username' placeholder='密碼' onChange={this.onChange.bind(this, 'password')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='email'>電郵地址</Label>
                        <Input type='email' name='email' id='email' placeholder='電郵地址' onChange={this.onChange.bind(this, 'email')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='telephone'>聯絡電話號碼</Label>
                        <Input type='text' name='telephone' id='telephone' placeholder='聯絡電話號碼' onChange={this.onChange.bind(this, 'telephone')} />
                    </FormGroup>
                    <FormGroup>
                        <Label for='contact_person'>聯絡人姓名</Label>
                        <Input type='text' name='telephone' id='contact_person' placeholder='聯絡人姓名' onChange={this.onChange.bind(this, 'contact_person')} />
                    </FormGroup>
                    <FormGroup>
                        <CardForm fontSize={12} />
                    </FormGroup>
                    <Button form='register-form' color="secondary" type='submit' name='submit' id='submit'>提交</Button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to="/">
                        <Button color="secondary" >返回首頁</Button>
                    </Link>
                </Form>

                {
                    this.props.msg && <RegisterFailModal
                        msg={this.props.msg}
                        close={this.props.clearFailedMsg}
                    />
                }
                {
                    this.props.msg && <RegisterSuccessModal
                        msg={this.props.msg}
                        close={this.props.clearFailedMsg}
                    />
                }
            </div>
        )
    }
}



// const mapStateToProps = (state: IRootState) => {
//     return {
//         msg: state.auth.msg
//     }
// }

// const mapDispatchToProps = (dispatch: ThunkDispatch) => {
//     return {
//         register: (user: any) => dispatch(register(user)),
//         clearFailedMsg: () => dispatch(clearFailedMsg())
//     }
// }

export default injectStripe(Register);
