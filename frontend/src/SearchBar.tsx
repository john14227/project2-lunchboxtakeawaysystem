import * as React from 'react'
import * as Autosuggest from 'react-autosuggest';
import { IRootState, ThunkDispatch } from './store';
import { loadLocation } from './home/thunks';
import { connect } from 'react-redux';
// import {themeable} from 'react-themeable-ts';
// import theme from 'theme.css';
import './theme.css'
import { clearSearchResult } from './home/actions';

interface ISearchProps {
    searchResult: string[]
    loadLocation: (nameOrAddress: string) => void
    clearSearchResult: () => void
}

interface ISearchState {
    value: string
    suggestions: any[]
}

// Imagine you have a list of languages that you'd like to autosuggest.
// const languages = [
//     {
//         name: 'ABC',
//         year: 1972
//     },
//     {
//         name: 'ABCDE',
//         year: 2012
//     },
//     {
//         name: 'XAE',
//         year: 2012
//     },
// ];

// Teach Autosuggest how to calculate suggestions for any given input value.
// const getSuggestions = (value: any) => {
//     const inputValue = value.trim().toLowerCase();
//     const inputLength = inputValue.length;

//     return inputLength === 0 ? [] : languages.filter(lang =>
//         lang.name.toLowerCase().slice(0, inputLength) === inputValue
//     );
// };

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
// const getSuggestionValue = (suggestion: any) => suggestion.name;

// Use your imagination to render suggestions.
// const renderSuggestion = (suggestion: any) => (
//     <div style={
//         {
//             color: 'red',
//             float: 'left'
//         }
//     }>
//         {suggestion.name}
//     </div>
// );

class SearchBar extends React.Component<ISearchProps, ISearchState> {
    constructor(props: ISearchProps) {
        super(props);

        // Autosuggest is a controlled component.
        // This means that you need to provide an input value
        // and an onChange handler that updates this value (see below).
        // Suggestions also need to be provided to the Autosuggest,
        // and they are initially empty because the Autosuggest is closed.
        this.state = {
            value: '',
            suggestions: []
        };
    }

    private getSuggestionValue = (suggestion: any) => suggestion.name;

    private getSuggestions = () => {
        // const inputValue = value.trim().toLowerCase();
        // const inputLength = inputValue.length;

        return this.props.searchResult || [];
    };

    private onChange = async (event: React.FormEvent<any>, { newValue = 0 }: any) => {
        this.setState({
            value: newValue,
        });
        if (newValue.length > 0 && /[\S]/g.test(newValue)){
            await this.props.clearSearchResult();
            await this.props.loadLocation(newValue);

        }
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    private onSuggestionsFetchRequested = ({ value }: any) => {
        this.onSuggestionsClearRequested();
        this.setState({
            suggestions: this.getSuggestions()
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    private onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    private onSuggestionSelected = (suggestion: any) => {
        const a = console;
        a.log(suggestion);
        localStorage.setItem('selected_option', suggestion.name);
    }

    private renderSuggestion = (suggestion: any) => (
        <div>
            <button>
                preview map
            </button>
            <div>
                {suggestion.name}
            </div>
        </div>


    );

    public render() {
        const { value, suggestions } = this.state;

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: '輸入自取點名稱或地址',
            value,
            onChange: this.onChange
        };

        const theme =
        {
            container: 'react-autosuggest__container',
            containerOpen: 'react-autosuggest__container--open',
            input: 'react-autosuggest__input',
            inputOpen: 'react-autosuggest__input--open',
            inputFocused: 'react-autosuggest__input--focused',
            suggestionsContainer: 'react-autosuggest__suggestions-container',
            suggestionsContainerOpen: 'react-autosuggest__suggestions-container--open',
            suggestionsList: 'react-autosuggest__suggestions-list',
            suggestion: 'react-autosuggest__suggestion',
            suggestionFirst: 'react-autosuggest__suggestion--first',
            suggestionHighlighted: 'react-autosuggest__suggestion--highlighted',
            sectionContainer: 'react-autosuggest__section-container',
            sectionContainerFirst: 'react-autosuggest__section-container--first',
            sectionTitle: 'react-autosuggest__section-title'
        }

        // const theme = {
        //     container: 'autosuggest',
        //     input: 'form-control',
        //     suggestionsContainer: 'dropdown open',
        //     suggestionsList: 'dropdown-menu',
        //     suggestion: '',
        //     suggestionFocused: 'active',
        //   };

        // Finally, render it!
        return (

            <div>
                <Autosuggest
                    suggestions={suggestions}
                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                    onSuggestionSelected={this.onSuggestionSelected}
                    getSuggestionValue={this.getSuggestionValue}
                    renderSuggestion={this.renderSuggestion}
                    inputProps={inputProps}
                    theme={theme}
                />
            </div>

        );
    }
}


const mapStateToProps = (state: IRootState) => {
    const searchResult = state.home.searchResult
    return {
        searchResult
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return {
        loadLocation: (nameOrAddress: string) => dispatch(loadLocation(nameOrAddress)),
        clearSearchResult: () => dispatch(clearSearchResult())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)