import * as express from 'express';
//import {Request,Response} from 'express'
import * as bodyParser from 'body-parser';
import * as expressSession from 'express-session';
import * as Knex from 'knex'; // Import knex library
import * as cors from 'cors';
// import * as aws from 'aws-sdk';
// import * as multer from 'multer';
// import * as multerS3 from 'multer-s3';
import * as passport from 'passport';
const knexConfig = require('./knexfile'); // Import knex config
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
import { UserService } from './services/UserService';
import { UserRouter } from './routers/UserRouter';
import { AdminService } from './services/AdminService';
// import { AdminRouter } from './routers/AdminRouter';
import * as Stripe from 'stripe';


const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!)

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'takeawaylunchboxsystem',
    resave: true,
    saveUninitialized: true,
    // cookie:{httpOnly: true,
    //         expires: expiresTime}
}));


app.use(passport.initialize())
app.use(passport.session())

import './passport';

import { MenuService } from './services/MenuService';
import { MenuRouter } from './routers/MenuRouter';
import { PromotionService } from './services/PromotionService';
import { PromotionRouter } from './routers/PromotionRouter';
import { KOLService } from './services/KOLService';
import { CreditCardService } from './services/CreditCardService';
import { PickUpPointService } from './services/PickUpPointService';
import { PickUpPointRouter } from './routers/PickUpPointRouter';
import { OrderService } from './services/OrderService';
import { OrderRouter } from './routers/OrderRouter';
import { OrderHistoryService } from './services/OrderHistoryService';
import { OrderStatusService } from './services/OrderStatusService';
// import passport = require('passport');

// function isLogined  (req:Request,res:Response,next:express.NextFunction){
//     if (req.session && req.session.token){
//         next();
//     } else{
//         res.status(401).json('You need to login first');
//     }
// }


app.use(express.static('public'));

// const s3 = new aws.S3({
//     accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//     secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//     region: 'ap-southeast-1'
// });


// const upload = multer({
//     storage: multerS3({
//         s3: s3,
//         bucket: 'cdn.hkorderfood.tk',
//         metadata: (req, file, cb) => {
//             cb(null, { fieldName: file.fieldname });
//         },
//         key: (req, file, cb) => {
//             cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
//         }
//     })
// })

export const adminService = new AdminService(knex);
export const userService = new UserService(knex);
const creditCardService = new CreditCardService(knex);
const menuService = new MenuService(knex);
const promotionService = new PromotionService(knex);
const kolService = new KOLService(knex);
const pickUpPointService = new PickUpPointService(knex);
const orderService = new OrderService(knex, stripe);
const orderHistoryService = new OrderHistoryService(knex);
const orderStatusService = new OrderStatusService(knex);

const userRouter = new UserRouter(userService, creditCardService, stripe);
const menuRouter = new MenuRouter(menuService, kolService);
const orderRouter = new OrderRouter(orderService, orderHistoryService, orderStatusService);
const promotionRouter = new PromotionRouter(promotionService);
// const adminRouter = new AdminRouter(adminService, upload);
const pickUpPointRouter = new PickUpPointRouter(pickUpPointService);


app.use('/', userRouter.router());
app.use('/orders', passport.authenticate('jwt', { session: false }), orderRouter.router());
app.use('/promotion', promotionRouter.router());
app.use('/menu', menuRouter.router());
// app.use('/admin', adminRouter.router());
app.use('/location', pickUpPointRouter.router());


const PORT = 8080
app.listen(PORT, () => {
    console.log(`listening to port ${PORT}`);
});
