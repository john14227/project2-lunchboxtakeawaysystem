import * as Knex from "knex";

async function users(knex: Knex) {
    await knex.schema.createTable('users', (table) => {
        table.increments().primary();
        table.string('username').notNullable();
        table.string('password').notNullable()
        table.string('email').notNullable();
        table.integer('telephone').notNullable();
        table.string('contact_person').notNullable();
        table.float('credit').defaultTo(0);
        table.string('stripeCustomerId');
        table.timestamps(false, true);
    });
}

async function creditCard(knex: Knex) {
    await knex.schema.createTable('credit_card', (table) => {
        table.increments().primary();
        table.string('credit_card_num');
        table.integer('user_id');
        table.foreign('user_id').references('users.id');
        table.timestamps(false, true);
    })
}

async function kolInfo(knex: Knex) {
    await knex.schema.createTable('kol_info', (table) => {
        table.increments().primary();
        table.string('fb_link');
        table.string('ig_link');
        table.timestamps(false, true);
    });
}

async function pickUpPoint(knex: Knex) {
    await knex.schema.createTable('pick_up_point', (table) => {
        table.increments().primary();
        table.string('name');
        table.string('address');
        table.float('x_latlng');
        table.float('y_latlng');
        table.timestamps(false, true);
    });
}


async function discount(knex: Knex) {
    await knex.schema.createTable('discount', (table) => {
        table.increments().primary();
        table.text('description');
        table.string('code');
        table.float('discount_offer');
        table.date('start_date');
        table.string('expired_date');
        table.timestamps(false, true);
    });
}

async function menu(knex: Knex) {
    await knex.schema.createTable('menu', (table) => {
        table.increments().primary();
        table.string('food');
        table.float('price');
        table.string('img_url');
        table.time('order_deadline');
        table.string('kol_intro_video');
        table.integer('kol_id');
        table.foreign('kol_id').references('kol_info');
        table.integer('discount_id');
        table.foreign('discount_id').references('discount.id')
        table.timestamps(false, true);
    });
}


async function orders(knex: Knex) {
    await knex.schema.createTable('orders', (table) => {
        table.increments().primary();
        table.string('tracking_number').notNullable();
        table.float('bill_amount');
        table.text('special_requirement');
        table.integer('user_id');
        table.foreign('user_id').references('users.id');
        table.integer('pick_up_point_id');
        table.foreign('pick_up_point_id').references('pick_up_point.id')
        table.timestamps(false, true);
    });
}

async function lunchBox(knex: Knex) {
    await knex.schema.createTable('lunch_box', (table) => {
        table.increments().primary();
        table.jsonb('QR_code');
        table.integer('order_id');
        table.foreign('order_id').references('orders.id');
        table.timestamps(false, true);
    })
}

async function orderHistory(knex: Knex) {
    await knex.schema.createTable('order_history', (table) => {
        table.increments().primary();
        table.integer('menu_id');
        table.integer('quantity');
        table.foreign('menu_id').references('menu.id');
        table.integer('order_id');
        table.foreign('order_id').references('orders.id');
        table.timestamps(false, true);
    })
}

async function orderStatus(knex: Knex) {
    await knex.schema.createTable('order_status', (table) => {
        table.increments().primary();
        table.string('status');
        table.integer('order_id');
        table.foreign('order_id').references('orders.id');
        table.timestamps(false, true);
    })
}

async function promotion(knex: Knex) {
    await knex.schema.createTable('promotion', (table) => {
        table.increments().primary();
        table.string('url');
        table.string('img');
        table.timestamps(false, true);
    });
}

async function admin(knex: Knex) {
    await knex.schema.createTable('admin', (table) => {
        table.increments().primary();
        table.string('username').notNullable();
        table.string('password').notNullable()
        table.string('email');
        table.integer('telephone');
        table.string('contact_person').notNullable();
        table.timestamps(false, true);
    });
}




export async function up(knex: Knex): Promise<any> {
    await users(knex);
    await creditCard(knex);
    await kolInfo(knex);
    await pickUpPoint(knex);
    await discount(knex);
    await menu(knex);
    await orders(knex);
    await lunchBox(knex);
    await orderHistory(knex);
    await orderStatus(knex);
    await promotion(knex);
    await admin(knex);

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists('admin');
    await knex.schema.dropTableIfExists('promotion');
    await knex.schema.dropTableIfExists('order_status');
    await knex.schema.dropTableIfExists('order_history');
    await knex.schema.dropTableIfExists('lunch_box');
    await knex.schema.dropTableIfExists('orders');
    await knex.schema.dropTableIfExists('menu');
    await knex.schema.dropTableIfExists('discount');
    await knex.schema.dropTableIfExists('pick_up_point');
    await knex.schema.dropTableIfExists('kol_info');
    await knex.schema.dropTableIfExists('credit_card');
    await knex.schema.dropTableIfExists('users');
}

