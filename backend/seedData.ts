import { hash } from './hash'



export async function createUser() {
    const user1 = {
        username: 'terry',
        password: await hash('abcde'),
        email: 'terry@uek.com',
        telephone: 12345678,
        contact_person: 'terry',
    }

    const user2 = {
        username: 'apple',
        password: await hash('123456'),
        email: 'apple@uek.com',
        telephone: 65432198,
        contact_person: 'apple',
    }

    const user3 = {
        username: 'apple2',
        password: await hash('apple2'),
        email: 'apple@uek.com',
        telephone: 65432198,
        contact_person: 'apple',
        stripeCustomerId: 'cus_F2ZPlsGguERftZ'

    }
    const user = [user1, user2, user3];
    return user;
}

export function createCreditCard() {
    const card1 = {
        user_id: 1,
        credit_card_num: 123456
    }

    const card2 = {
        user_id: 2,
        credit_card_num: 321654
    }

    const card3 = {
        user_id: 1,
        credit_card_num: 987654
    }
    const card = [card1, card2, card3]
    return card;
}

export function createKol() {
    const kol1 = {
        fb_link: 'https://www.facebook.com/users/Macy',
        ig_link: 'https://www.instagram.com/users/Macy',
    }

    const kol2 = {
        fb_link: 'https://www.facebook.com/users/Jason',
        ig_link: 'https://www.instagram.com/users/Jason',
    }

    const kol = [kol1, kol2];
    return kol;
}

export function createPoint() {
    const point1 = {
        name: 'Nova cafe',
        address: '掃管笏瑜翠街2號屯門愉翠街2號 A室高層地下蟠龍半島87座',
        x_latlng: 22.367381,
        y_latlng: 113.996105
    }

    const point2 = {
        name: 'The Silk Road Cafe',
        address: 'Crossroads Village, 2 Castle Peak Road, Tuen Mun',
        x_latlng: 22.374028,
        y_latlng: 113.992597
    }

    const point3 = {
        name: '喵一下 Garden Meow',
        address: '中環皇后大道中35號',
        x_latlng: 22.282304,
        y_latlng: 114.156408,
    }

    const point4 = {
        name: '吃什麼 What to Eat',
        address: '中環皇后大道中59號',
        x_latlng: 22.283108,
        y_latlng: 114.155622,
    }


    const points = [point1, point2, point3, point4];
    return points;
}

export function createDiscount() {
    const discount1 = {
        description: '9折',
        code: 'WQ123VV2C1DQSW',
        discount_offer: 0.9,
        start_date: '2019-5-20',
        expired_date: '2019-6-20'
    }

    const discount2 = {
        description: '8折',
        code: '12FDGFD56298QSW',
        discount_offer: 0.8,
        start_date: '2019-5-30',
        expired_date: '2019-6-10'
    }
    const discount = [discount1, discount2];
    return discount;
}

export function createMenu() {
    const setA = {
        food: '避風塘軟殼蟹 (4件)',
        price: 35,
        img_url: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557212165708.jpeg',
        order_deadline: '10:00',
        kol_intro_video: '',
        discount_id: 2,
        kol_id: 1
    }

    const setB = {
        food: '芝士溫泉蛋咖哩海鮮焗飯',
        price: 35,
        img_url: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557212333573.jpeg',
        order_deadline: '10:00',
        kol_intro_video: '',
        discount_id: 2,
        kol_id: 2
    }

    const setC = {
        food: '芝士焗牛柳粒飯配紅酒牛扒汁',
        price: 38,
        img_url: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557211970727.jpeg',
        order_deadline: '10:00',
        kol_intro_video: '',
        discount_id: 2,
        kol_id: 2
    }

    const setD = {
        food: '香茅豬扒蛋飯 ',
        price: 36,
        img_url: 'https://s3.ap-southeast-1.amazonaws.com/cdn.hkorderfood.tk/foodpic-1557212093446.jpeg',
        order_deadline: '10:00',
        kol_intro_video: '',
        discount_id: 2,
        kol_id: 2
    }

    const menu = [setA, setB, setC, setD];
    return menu;
}

export function createPromtion() {
    const promotion = {
        url: 'http://google.com',
        img: 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
    }
    return [promotion];
}

export function createOrders() {
    const order1 = {
        tracking_number: '4370551',
        bill_amount: 32.6,
        special_requirement: '少飯',
        user_id: '1',
        pick_up_point_id: '1',
    }

    const order2 = {
        tracking_number: '2211128',
        bill_amount: 40.2,
        special_requirement: '多飯',
        user_id: '1',
        pick_up_point_id: '1',
    }

    const order3 = {
        tracking_number: '3418008',
        bill_amount: 32.6,
        special_requirement: '多汁',
        user_id: '2',
        pick_up_point_id: '2',
    }
    const order4 = {
        tracking_number: '9323231',
        bill_amount: 45,
        special_requirement: '',
        user_id: '2',
        pick_up_point_id: '2',
    }

    const order = [order1, order2, order3, order4];
    return order;
}


// export function createPoint2() {
//     const point1 = {
//         name: '喵一下 Garden Meow',
//         address: '中環皇后大道中35號',
//         x_latlng: '22.282304',
//         y_latlng: '114.156408',
//     }

//     const point2 = {
//         name: '吃什麼 What to Eat',
//         address: '中環皇后大道中59號',
//         x_latlng: '22.283108',
//         y_latlng: '114.155622',
//     }

//     const point = [point1, point2];
//     return point;
// }


export function createOrderHistory() {
    const history1 = {
        quantity: 2,
        order_id: '1',
        menu_id: '1',
    }

    const history2 = {
        quantity: 3,
        order_id: '2',
        menu_id: '1',
    }

    const history3 = {
        quantity: 1,
        order_id: '2',
        menu_id: '2',
    }


    const history = [history1, history2, history3];
    return history;
}

export function createOrderStatus() {
    const orderstatus1 = {
        status: '待取',
        order_id: '1',
    }
    const orderstatus2 = {
        status: '送往自取點',
        order_id: '2',
    }



    const orderstatus = [orderstatus1, orderstatus2];
    return orderstatus;
}