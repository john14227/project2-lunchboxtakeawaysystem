import * as Knex from "knex";
import {
    createUser, createKol,
    createDiscount, createMenu, createPromtion, createCreditCard, createOrders, createOrderHistory, createPoint, createOrderStatus
} from '../seedData'

const creditCard = createCreditCard();
const kol = createKol();
const points = createPoint();
const discount = createDiscount();
const menu = createMenu();
const promotion = createPromtion();
const orders = createOrders();
const history = createOrderHistory();
const orderstate = createOrderStatus();


async function userData(knex: Knex) {
    const user = await createUser();
    await knex.batchInsert('users', user, 30);
}

async function creditCardData(knex: Knex) {
    await knex.batchInsert('credit_card', creditCard, 30);
}

async function kolInfoData(knex: Knex) {
    await knex.batchInsert('kol_info', kol, 30);
}

async function pickUpPointData(knex: Knex) {
    await knex.batchInsert('pick_up_point', points, 30);

}

async function discountData(knex: Knex) {
    await knex.batchInsert('discount', discount, 30);
}

async function menuData(knex: Knex) {
    await knex.batchInsert('menu', menu, 30);
}

async function promotionData(knex: Knex) {
    await knex.batchInsert('promotion', promotion, 30)
}

async function Orders(knex: Knex) {
    await knex.batchInsert('orders', orders, 30)
}

async function Order_history(knex: Knex) {
    await knex.batchInsert('order_history', history, 30)
}


async function Order_state(knex: Knex) {
    await knex.batchInsert('order_status', orderstate, 30)
}

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    // await knex.del('promotion');
    // await knex.del('orderHistory');
    // await knex.del('lunchBox');
    // await knex.del('orders');
    // await knex.del('menu');
    // await knex.del('discount');
    // await knex.del('pickUpPoint');
    // await knex.del('kolInfo');
    // await knex.del('food');
    // await knex.del('users');

    await userData(knex);
    await creditCardData(knex);
    await kolInfoData(knex);
    await pickUpPointData(knex);
    await discountData(knex);
    await menuData(knex);
    await promotionData(knex);
    await Orders(knex);
    await Order_history(knex);
    await Order_state(knex);
};
