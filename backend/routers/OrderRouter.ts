import * as express from 'express';
// import { Request, Response } from 'express';
import { OrderService } from '../services/OrderService';
import { OrderHistoryService } from '../services/OrderHistoryService'
import { OrderStatusService } from '../services/OrderStatusService';


export class OrderRouter {
    constructor(private orderService: OrderService,
        private orderHistoryService: OrderHistoryService,
        private orderStatusService: OrderStatusService) {
    }
    router() {
        const router = express.Router();
        router.get('/currentUser', this.getUserOrders);
        router.post('/submit', this.postOrder)
        return router;
    }

    getUserOrders = async (req: any, res: any) => {
        try {
            // const userId = 1;
            console.log('req.user: ', req.user);
            const userId = req.user.id;
            console.log('userId: ', userId);
            const orders = await this.orderService.retrieveOrders(userId);
            await res.status(200).json({ orders: orders });
        } catch (e) {
            await res.status(500).json({ msg: 'get orders failed' });
        }
    }

    postOrder = async (req: any, res: any) => {
        try {
            const order = { ...req.body.order };
            console.log(order);
            order.user_id = req.user.id
            const orderHistory: any[] = [];
            const menuItems: any[] = order.menuItems;
            menuItems.forEach((item: any) => {
                orderHistory.push({
                    menu_id: item.id,
                    quantity: item.quantity
                })
            })
            delete order.menuItems;
            //console.log(menuItems);
            console.log('order', order);
            let trackingNumber = Math.random().toString(36).substring(2, 15);

            while (trackingNumber.length !== 11) {
                trackingNumber = Math.random().toString(36).substring(2, 15);
            }

            order.tracking_number = trackingNumber.toUpperCase();
            const orderId = await this.orderService.create(order)
            orderHistory.forEach((item: any) => {
                item.order_id = orderId
            });
            const orderStatus = {
                order_id: orderId,
                status: '準備中'
            }
            await this.orderStatusService.create(orderStatus);
            await this.orderHistoryService.create(orderHistory);
            await res.status(200).json({ msg: '訂單提交成功' });
        } catch (e) {
            console.log(e)
            await res.status(500).json({ msg: '訂單提交失敗' });
        }
    }
}