import {PickUpPointService} from '../services/PickUpPointService';
import * as express from 'express';
import {Request,Response} from 'express'

export class PickUpPointRouter{
    constructor(private pickUpPointService:PickUpPointService){
    }

    router(){
        const router = express.Router();
        router.post('/',this.getSimilarLocation);
        router.get('/',this.getAllLocation);
        return router;
    }

    private getSimilarLocation = async (req:Request,res:Response) =>{
        try{
            const nameOrAddress = req.body.nameOrAddress;
            const locationOptions = await this.pickUpPointService.getSimilarLocation(nameOrAddress);
            if (locationOptions.length == 0){
                console.log('Options',locationOptions);
                await res.status(200).json(['無相符結果']);   
            } else {
                console.log('Options',locationOptions);
                await res.status(200).json({locationOptions:locationOptions})
            }
            
        } catch(e){
            await res.status(500).json({msg:'Failed to get locations'})
        }
        
    }

    private getAllLocation = async (req:Request,res:Response) => {
        try{
            const allLocation = await this.pickUpPointService.getAllLocation();
            await res.status(200).json({allLocation:allLocation});
        }catch(e){
            await res.status(500).json({msg:'Failed to get locations'})
        }
    }
}
