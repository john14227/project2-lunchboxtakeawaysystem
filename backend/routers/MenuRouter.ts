import {MenuService} from '../services/MenuService';
import {KOLService} from '../services/KOLService'
import {Request,Response} from 'express'
import * as express from 'express';

export class MenuRouter {
    constructor(private menuService:MenuService,private kolService:KOLService){
    }

    router(){
        const router = express.Router();
        router.get('/',this.getMenu);
        router.get('/:id',this.getItem);
         // router.post('/',this.post);
        return router;
    }

    private getMenu = async (req:Request,res:Response) =>{
        try {
            const menu = await this.menuService.getMenu();
            const allKol = await this.kolService.retrieve();

            menu.forEach((item: any) => {
                allKol.forEach((kol:any) => {
                    if (item.kol_id == kol.id){
                        item.kol_fb_link = kol.fb_link;
                        item.kol_ig_link = kol.ig_link;
                        delete item.kol_id;
                    }
                });
            });

            await res.status(200).json({menu:menu});
        } catch (e){
            console.error(e);
            res.status(500).json({msg:'Failed to get menu'});
        }
    }

    private getItem = async (req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id,10);
            console.log(req.params.id);
            const item = await this.menuService.getItem(id);
            res.status(200).json(item);
        } catch (e){
            res.status(500).json({msg:'Failed to load item'});
        }
        
    }

    // private post = async (req:Request,res:Response) =>{
    //     try {

    //     } catch (e){

    //     }
    // }


}