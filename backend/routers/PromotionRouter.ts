import {Request,Response} from 'express';
import * as express from 'express';
import {PromotionService} from '../services/PromotionService';


export class PromotionRouter {
    constructor(private promotionService:PromotionService){
    }

    router(){
        const router = express.Router();
        router.get('/',this.get);
        return router;
    }

    private get = async (req:Request,res:Response) =>{
        try{
            const promotion = await this.promotionService.getPromotion();
            res.status(200).json({promotion:promotion});
        }catch(e){
            res.status(500).json({promotionMsg:'Failed to load promotion Information'})
        }
    }
}