import * as express from 'express';
import { Request, Response } from 'express';
import { UserService } from '../services/UserService'
import { checkPassword } from '../hash'
//import fetch from 'node-fetch';
import * as jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { hash } from '../hash'
import { CreditCardService } from '../services/CreditCardService';
import * as passport from 'passport';
import * as Stripe from 'stripe'
export class UserRouter {
    constructor(private userService: UserService, private creditCardService: CreditCardService, private stripe: Stripe) {
    }

    router() {
        const router = express.Router();
        router.post('/login', this.login);
        router.post('/fblogin', this.loginFacebook);
        router.get('/logout', this.logout);
        router.post('/register', this.register);
        router.use('/user', passport.authenticate('jwt', { session: false }), this.protectedUserActivities());

        return router;
    }

    // Protected for users only


    private protectedUserActivities = () => {
        const router = express.Router();
        router.get('/currentUser', this.getCurrentUser);
        router.post('/placeorder', this.placeOrder);
        router.get('/showhistory', this.showOrderHistory);
        router.put('/currentUser', this.updateCurrentUser)
        return router;
    }

    private getCurrentUser = async (req: any, res: any) => {
        try {
            const userId = req.user.id;
            const user = await this.userService.retrieveById(userId);
            console.log('user:',user)
            await res.status(200).json({user:user});
        } catch (e) {
            await res.status(500).json('Failed to load user info');
        }
    }


    private updateCurrentUser = async (req: any, res: any) => {
        try {

            const userId = req.user.id;
            console.log(userId);
            const user = { ...req.body.user };
            console.log(user);
            let creditCardInfo = {};
            const creditCards = user.credit_card_num;
            console.log(creditCards);
            if (typeof (creditCards) === 'number') {
                creditCardInfo = {
                    credit_card_num: user.credit_card_num,
                    user_id: userId
                };
                await this.creditCardService.update([creditCardInfo], userId);
            } else {
                let creditCardNumbers: any[] = [];
                for (let creditCard of creditCards) {
                    creditCardNumbers.push({
                        credit_card_num: creditCard,
                        user_id: userId
                    })
                };
                await this.creditCardService.update(creditCardNumbers, userId);
            }

            delete user.credit_card_num;
            await this.userService.put(user, userId);
            await res.status(200).json('Update user success');
        } catch (e) {
            console.log(e);
            await res.status(500).json('Failed to update user ');
        }
    }

    // Open for public 

    private login = async (req: any, res: any) => {
        if (req.body.username && req.body.password) {
            const { username, password } = req.body;
            const users = (await this.userService.retrieve(username))
            const user = users[0];
            if (user && await checkPassword(password, user.password)) {
                const payload = {
                    id: user.id
                };
                const token = jwtSimple.encode(payload, jwt.jwtSecret);
                console.log('req.user: ', req.user);
                res.json({
                    token: token
                });
            } else {
                res.status(401).json({ msg: "Username/Password is wrong!" });
            }
        } else {
            res.status(401).json({ msg: "Username/Password is wrong!" });
        }
    }

    private register = async (req: any, res: any) => {
        try {
            let user = { ...req.body }
            console.log(user);


            const customer = await this.stripe.customers.create({
                description: `Customer for ${user.username}`,
                email: user.email,
            });

            const source = await (this.stripe as any).sources.create({
                type: 'card',
                currency: 'hkd',
                owner: {
                    email: user.email,
                },
                token: user.credit_card_num,
            });

            await this.stripe.customers.update(customer.id, {
                source: source.id
            });
            const stripeCustomerId = customer.id;

            user = { ...user, stripeCustomerId };

            const creditCardNumber = {} as any;
            creditCardNumber.credit_card_num = user.credit_card_num;
            if (user.credit_card_num) {
                delete user.credit_card_num
            }
            user.password = await hash(user.password)

            const userId = await this.userService.create(user);
            console.log('userId:', userId);
            if (creditCardNumber.credit_card_num != undefined) {
                creditCardNumber.user_id = userId
                // const allCardNumber = creditCardNumber.credit_card_num

                await this.creditCardService.create([creditCardNumber]);


            }

            await res.status(200).json({ msg: 'Registeration success!' })
        }
        catch (e) {
            console.log(e);
            await res.status(500).json({ msg: 'Registeration failed!' })
        }
    }

    private placeOrder = async (req: any, res: any) => {
        try {
            const user_id = req.session.userId;
            // const user_id = 6;
            const temp_number = (Math.floor(Math.random() * 9999999)).toString(10);
            // console.log("1:", temp_number, temp_number.length);
            const getNumber = await this.userService.checkTrackingNumber(temp_number)

            while (getNumber[0] !== undefined || temp_number.length < 7) {
                const temp_number = (Math.floor(Math.random() * 9999999)).toString(10);
                const getNumber = await this.userService.checkTrackingNumber(temp_number);
                console.log(getNumber);
            }

            const tracking_number = temp_number;
            // console.log("2:", tracking_number, temp_number.length);
            const order = { ...req.body, user_id, tracking_number }
            await this.userService.placeOrder(order)
            res.json({ success: true })
        } catch (e) {
            console.log(e)
            res.json({ success: false })
        }
    }

    private showOrderHistory = async (req: any, res: any) => {
        try {
            // const id = req.session.userId;
            const id = req.body.id;
            const getOrders = await this.userService.retrieveOrdersHistory(id);

            res.json(getOrders);
        } catch (e) {
            console.log(e)
            res.json({ success: false })
        }



    }

    private loginFacebook = async (req: Request, res: Response) => {
        if (req.body.token) {
            const facebookToken = req.body.token;
            const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${facebookToken}&fields=id,name,email,picture`);
            const result = await fetchResponse.json();
            console.log(result);
            if (result.error) {
                res.status(401).json({ msg: "Token is wrong!" });
                return;
            }

            let user = (await this.userService.retrieveUsers()).find((user: any) => user.email == result.email);

            // Create a new user if the user does not exist
            if (!user) {
                const password = Math.random().toString(36)
                const hashedpassword = await hash(password);
                user = (await this.userService.create({
                    username: result.username,
                    password, hashedpassword,
                    email: result.email
                }))[0];
            }
            const payload = {
                id: user.id
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token
            });
        } else {
            res.status(401).json({ msg: "Token is wrong!" });
        }
        return;
    }


    private logout = async (req: Request, res: Response) => {
        try {
            await req.logOut();
            await res.status(200).json('logout success');
        } catch (e) {
            res.status(500).json('internal server error')
        }

    }
}