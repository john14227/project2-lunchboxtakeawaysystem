import * as express from 'express';
import { Request, Response } from 'express';
import { AdminService } from '../services/AdminService'
import * as multer from 'multer';
// import fetch from 'cross-fetch';

export class AdminRouter {
    constructor(private adminService: AdminService, private upload: multer.Instance) {
    }

    router() {
        const router = express.Router();
        router.post('/addpoint', this.addPickupPoint);
        router.post('/addfood', this.upload.single('foodpic'), this.addMenu);
        router.post('/register', this.register);

        return router;
    }

    private addPickupPoint = async (req: Request, res: Response) => {
        try {
            const point = { ...req.body };
            await this.adminService.addPickupPoint(point);
            res.json({ success: true })
        } catch (e) {
            console.log(e)
            res.json({ success: false })
        }

    }

    private addMenu = async (req: Request, res: Response) => {
        try {
            // console.log(req.body, req.files)
            // const id = parseInt(req.body.id, 10);
            const img_url = (req.file as any).location;
            const addfood = { ...req.body, img_url };
            await this.adminService.insertFoodInfo(addfood);
            res.json({ isSuccess: true });
        } catch (e) {
            res.json({ isSuccess: false, msg: e.toString() });
        }

    }

    private register = async (req: Request, res: Response) => {
        try {
            const user = { ...req.body }
            await this.adminService.createAccount(user)
            res.json({ success: true })
        }
        catch (e) {
            console.log(e);
            res.json({ success: false })
        }
    }

}
