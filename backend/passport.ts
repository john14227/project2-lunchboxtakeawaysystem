import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';
import jwt from './jwt';
// The userService is also the same as previous in BAD008
import { userService } from './main';


// passport.initialize();

const  JWTStrategy = passportJWT.Strategy;
const {ExtractJwt} = passportJWT;

passport.use('jwt',new JWTStrategy({
    secretOrKey: jwt.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
},async (payload,done)=>{
    const user = (await userService.retrieveUsers())
            .find((user:any)=>user.id == payload.id);
    if(user){
        return done(null,{ id : user.id});
    } else {
        return done(new Error("User not Found"),null);
    }
})
);