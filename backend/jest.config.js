// module.exports = {
//   "roots": [
//     "<rootDir>/src"
//   ],
//   "transform": {
//     "^.+\\.tsx?$": "ts-jest"
//   },
//   "moduleFileExtensions": [
//     "ts",
//     "tsx",
//     "js",
//     "jsx",
//     "json",
//     "node"
//   ],
// }

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
};