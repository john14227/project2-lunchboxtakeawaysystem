import * as Knex from 'knex'
import * as moment from 'moment'
//const moment = require('moment')
import * as Stripe from 'stripe'



export class OrderService {
    constructor(private knex: Knex, private stripe: Stripe) {
    }

    async retrieveOrders(userId: number) {
        const orders = await this.knex.select('*').from('orders').where('user_id', userId);
        const orderId: any[] = [];
        const pickUpPointId: any[] = []
        moment.locale('zh-hk');
        orders.forEach((order: any) => {
            // console.log(order.id)
            orderId.push(Number(order.id));
            pickUpPointId.push(Number(order.pick_up_point_id));
            order.createdDate = moment(order.created_at).format('ll')
            order.createdTime = moment(order.created_at).format('LT')
        });
        console.log(orderId);
        const orderHistories = await this.knex.select('*').from('order_history').whereIn('order_id', orderId);
        const pickUpPoints = await this.knex.select('*').from('pick_up_point').whereIn('id', pickUpPointId);
        console.log('orderHistories', orderHistories);

        for (let order of orders) {
            let menuItemsId = [];
            for (let orderHistory of orderHistories) {
                if (order.id == orderHistory.order_id) {
                    menuItemsId.push(orderHistory.menu_id)
                }

            }
            const menuItems = await this.knex.select('id', 'food', 'price').from('menu').whereIn('id', menuItemsId);
            order.menuItems = menuItems;
        }


        for (let orderHistory of orderHistories) {
            for (let order of orders) {
                const menuItems = order.menuItems;
                for (let menuItem of menuItems) {
                    // console.log('menuItem',menuItem);
                    // console.log('orderHistory',orderHistory)
                    if (orderHistory.menu_id == menuItem.id) {
                        menuItem.quantity = orderHistory.quantity
                    }
                }

                //console.log(menuItem);
            }
        }

        orders.forEach((order: any) => {
            pickUpPoints.forEach((pickUpPoint: any) => {
                if (order.pick_up_point_id == pickUpPoint.id) {
                    order.pickUpPoint = pickUpPoint.name;
                    delete order.pick_up_point_id;
                }
            })
        });

        console.log('menu: ', orders);
        return orders;
    }

    async create(order: any) {
        // get token by order.user_id using knex
        const getToken = await this.knex.select('stripeCustomerId').from('users').where('id', order.user_id);
        console.log(getToken);

        // when you have the token, use this.stripe.charges.create to get pcaid, bear in mind amount maybe get from order.bill_amount 

        let { status } = await this.stripe.charges.create({
            amount: order.bill_amount * 100,
            currency: "hkd",
            description: "An example charge",
            customer: getToken[0].stripeCustomerId
        });
        console.log({ status });

        const orderRecord = await this.knex.transaction(async (trx: Knex.Transaction) => {
            return await trx.insert(order).into('orders').returning('id');
        })



        const orderId = orderRecord[0];
        return orderId;



    }
}

// const knexConfig = require('../knexfile');
// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

// const a = new OrderService (knex);
// async function abc (){
//     const orders = (await a.retrieveOrders(1))
//     console.log( orders[1].menuItems);
// }

// abc();