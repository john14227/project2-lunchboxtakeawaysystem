import * as Knex from 'knex'
// const knexConfig = require('../knexfile');
// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

export class PromotionService{
    constructor(private knex:Knex){
    }

    async getPromotion(){
        return await this.knex.select('*').from('promotion');
    }

    async post(){

    }

    async update(){

    }

    async delete(){

    }
}

// const a = new PromotionService (knex);
// async function abc (){
//     console.log(await a.getPromotion());
// }

// abc();