import * as Knex from 'knex'

export class UserService {
    constructor(private knex: Knex) {

    }

    async retrieve(username: string) {
        return await this.knex.select('*').from('users').where('username', username);
    }

    async retrieveById(id:number) {
        const user = await this.knex.first('id','username','email','telephone','contact_person').from('users').where('id', id);
        // const creditCard = await this.knex.select('credit_card_num').from('credit_card').where('user_id',id);
        // user.credit_card_num = creditCard
        return user;
    }

    async retrieveUsers() {
        return await this.knex.select('*').from('users');
    }

    async create(user: any) {
       // return await this.knex.insert(user).into('users').returning("id");

        return await this.knex.transaction(async(trx:Knex.Transaction)=>{
            const userId = await trx.insert(user).into('users').returning("id");
            return Number(userId[0]);
        })
        
    }

    async put(user:any,userId:number) {
        await this.knex('users').update(user).where('id',userId);
    }

    async delete() {

    }

    async placeOrder(order: any) {
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.insert(order).into('orders');
        })
        

    }

    async checkTrackingNumber(trackingNumber: string) {
        return await this.knex.select('*').from('orders').where('tracking_number', trackingNumber);

    }

    // async retrieveOrderHistory() {
    //     await this.knex.select('*').from('orders').where('user_id')
    //     return await this.knex.select('*').from('order_history').where('order_id')

    // }

    async retrieveOrdersHistory(id: any) {
        const orders = await this.knex.select('tracking_number').from('users')
            .innerJoin('orders', 'orders.user_id', 'users.id')
            .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
            .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
            .innerJoin('menu', 'menu.id', 'order_history.menu_id')
            .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('tracking_number')
            .where('users.id', id);

        const trackingNumbers = [] as any;
        orders.forEach(function (order: any) {
            trackingNumbers.push(order.tracking_number)
        })


        for (let trackingNumber of trackingNumbers) {
            const tracking_number = await this.knex.select('tracking_number').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('tracking_number')
                .where('tracking_number', trackingNumber);


            const bill_amount = await this.knex.select('bill_amount').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('bill_amount')
                .where('tracking_number', trackingNumber);

            const special_requirement = await this.knex.select('special_requirement').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('special_requirement')
                .where('tracking_number', trackingNumber);

            const point_name = await this.knex.select('pick_up_point.point_name').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('pick_up_point.point_name')
                .where('tracking_number', trackingNumber);


            const point_address = await this.knex.select('pick_up_point.point_address').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('pick_up_point.point_address')
                .where('tracking_number', trackingNumber);

            const orderData = await this.knex.select('menu.food', 'menu.price').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id')
                .where('tracking_number', trackingNumber);


            const order_status = await this.knex.select('order_status.status').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('order_status.status')
                .where('tracking_number', trackingNumber);



            const updated_at = await this.knex.select('order_status.updated_at').from('users')
                .innerJoin('orders', 'orders.user_id', 'users.id')
                .innerJoin('pick_up_point', 'pick_up_point.id', 'orders.user_id')
                .innerJoin('order_history', 'order_history.order_id', 'pick_up_point.id')
                .innerJoin('menu', 'menu.id', 'order_history.menu_id')
                .innerJoin('order_status', 'order_status.order_id', 'orders.id').groupBy('order_status.updated_at')
                .where('tracking_number', trackingNumber);

            const fullList = { tracking_number, bill_amount, special_requirement, point_name, point_address, orderData, order_status, updated_at }
            return fullList; //一張單未出到兩個菜
        }
        return null;
    }


}

