import * as Knex from 'knex'
export class OrderStatusService {
    constructor(private knex:Knex){
    }

    async create (orderStatus:any){
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.insert(orderStatus).into('order_status');
        })
        
    }
}