import * as Knex from 'knex';

export class KOLService {
    constructor(private knex:Knex){

    }

    async retrieve (){
        return await this.knex.select('*').from('kol_info');
    }

    async create(kol:any){
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            trx.insert(kol).into('kol_info');
        })
        
    }

}