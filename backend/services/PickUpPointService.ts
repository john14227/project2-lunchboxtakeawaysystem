import * as Knex from 'knex';
// const knexConfig = require('../knexfile');
// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


export class PickUpPointService{
    constructor(private knex:Knex){
    }

    async getSimilarLocation(nameOrAddress:string){
        return await this.knex.select('id','name','address','x_latlng','y_latlng').from('pick_up_point').where('name','ilike',`%${nameOrAddress}%`);
    }

    async getAllLocation(){
        return await this.knex.select('id','name','address','x_latlng','y_latlng').from('pick_up_point');
    }
}


// const a = new PickUpPointService (knex);
// async function abc (){
//     console.log(await a.getSimilarLocation('fe'));
// }

// abc();