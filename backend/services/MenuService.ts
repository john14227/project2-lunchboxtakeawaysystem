import * as Knex from 'knex';
// const knexConfig = require('../knexfile');
// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

export class MenuService {
    constructor(private knex: Knex) {
    }

    async getItem(id:number){
        const item = await this.knex.first('*').from('menu').where('id',id);
        return item;
    }

    async getMenu() {
        const menu = await this.knex.select('*').from('menu');
        return menu;
    }
    

}

// const a = new MenuService (knex);
// async function abc (){
//     console.log(await a.getItem(1));
// }

// abc();