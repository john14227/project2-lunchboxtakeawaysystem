import * as Knex from 'knex'

export class CreditCardService {
    constructor(private knex: Knex) {

    }
    async create(creditCardInfo: any[]) {
        await this.knex.transaction(async (trx: Knex.Transaction) => {
            await trx.batchInsert('credit_card', creditCardInfo, 30);
            
        })
    }

    async update(creditCardInfo: any[], userId: number) {
        await this.knex.transaction(async (trx: Knex.Transaction) => {
            await trx('credit_card').where('user_id',userId).del();

            await trx.batchInsert('credit_card', creditCardInfo, 30);
            
        })

    }
}