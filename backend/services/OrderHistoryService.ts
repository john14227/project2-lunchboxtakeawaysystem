import * as Knex from 'knex';

export class OrderHistoryService {
    constructor(private knex:Knex){

    }

    async retrieve(orderId:number){
        return this.knex.select('*').from('order_history').where('order_id',orderId);
    }

    async create(orderHistory:any[]){
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.batchInsert('order_history',orderHistory,30)
        })
        
    }
}