import * as Knex from 'knex'

export class AdminService {
    constructor(private knex: Knex) {

    }

    async createAccount(user: any) {
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.insert(user).into('admin')
        })
        
    }

    async get(username: string) {
        return await this.knex.select('*').from('users').where('username', username);
    }

    async getAdmin() {
        return await this.knex.select('*').from('admin');
    }

    async addPickupPoint(point: any) {
        await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.insert(point).into('pick_up_point');
        })
        
    }

    async insertFoodInfo(addfood: any) {
        return await this.knex.transaction(async(trx:Knex.Transaction)=>{
            await trx.table('menu').insert(addfood);
        })
        
    }

    async post() {

    }

    async put() {

    }

    async delete() {

    }
}


