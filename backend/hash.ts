import * as bcrypt from 'bcrypt';

const salt = 10;
export async function hash(plainPassword:any){
    const hashPassword = await bcrypt.hash(plainPassword, salt);
    return hashPassword;
}

export async function checkPassword(plainPassword:any,hashPassword:any){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

